using UnityEngine;

[CreateAssetMenu(menuName = "Novasloth/Upgrades/ExplosiveRoundsUpgrade")]
public class ExplosiveRoundsUpgradeSO : UpgradeSO {

    /////////////////////////////////////////////////////////////////
    // A B S T R A C T   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // 
    public override void DoUpgrade (Weapon _Weapon) {
        _Weapon.ExplosiveUpgrade = true;
    }

    // 
    public override void UndoUpgrade (Weapon _Weapon) {
        _Weapon.ExplosiveUpgrade = false;
    }
}
