using UnityEngine;

// Novasloth Games LLC
// Lee Barton
[CreateAssetMenu(menuName = "Novasloth/Perks/Bro")]
public class BroPerkSO : PerkSO {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Range(0.0f, 1.0f)]
    [SerializeField] public float explosionDamageMultiplier = 0.25f;
    [Range(0.0f, 1.0f)]
    [SerializeField] public float explosionRadiusMultiplier = 0.50f;

    public override void Init () {
        Type = Perk.Enum.SUIT;
        Description = $"Bro's passive perk increases explosion damage and radius";
    }

    public override void Activate () {
        Debug.Log($"INSTANCE BEFORE: {Weapons.Instance.ExplosionRadiusMultiplier}");
        Weapons.Instance.ExplosionRadiusMultiplier += explosionRadiusMultiplier;
        Weapons.Instance.ExplosionDamageMultiplier += explosionDamageMultiplier;
        Debug.Log($"INSTANCE AFTER: {Weapons.Instance.ExplosionRadiusMultiplier}");
    }

    public override void Deactivate () {
        Debug.LogWarning("BroPerkSO :: Deactivate :: Bro's passive perk should not be deactivated!");
    }
}
