using Novasloth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
[CreateAssetMenu(menuName = "Novasloth/Perks/Camo")]
public class CamoPerkSO : PerkSO {

    public override void Init () {
        Type = Perk.Enum.CAMO;
        Description = $"Camo's passive perk ";
    }

    public override void Activate () {}

    public override void Deactivate () {
        Debug.LogWarning("CamoPerkSO :: Deactivate :: Camo's passive perk should not be deactivated!");
    }
}
