using UnityEngine;

// Novasloth Games LLC
// Lee Barton
[CreateAssetMenu(menuName = "Novasloth/Perks/PainDay")]
public class PainDayPerkSO : PerkSO {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private float damageIncrease = 1.00f; // 100%

    public override void Init () {
        Type = Perk.Enum.PAINDAY;
        Description = $"Increase damage of all weapons by {damageIncrease * 100}%";
    }

    public override void Activate () {
        Weapons.Instance.DamageModifier += damageIncrease;
    }

    public override void Deactivate () {
        Weapons.Instance.DamageModifier -= damageIncrease;
    }
}
