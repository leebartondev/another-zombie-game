using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
[CreateAssetMenu(menuName = "Novasloth/Perks/Karen")]
public class KarenPerkSO : PerkSO {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Range(0.0f, 1.0f)]
    [SerializeField] private float healingDelayPercentDecrease = 0.5f;

    private PlayerHealth _PlayerHealth;

    public override void Init () {
        _PlayerHealth = Util.GetPlayerByTag().GetComponent<PlayerHealth>();

        Type = Perk.Enum.CAMO;
        Description = $"Karen's passive perk regenerates health faster";
    }

    public override void Activate () {
        _PlayerHealth.HealDelay = (_PlayerHealth.HealDelay * healingDelayPercentDecrease);
    }

    public override void Deactivate () {
        Debug.LogWarning("KarenPerkSO :: Deactivate :: Karen's passive perk should not be deactivated!");
    }
}
