using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class DamageTile : MonoBehaviour, IDestructable {

    /////////////////////////////////////////////////////////////////
    // G L O B A L   V A R I A B L E S
    /////////////////////////////////////////////////////////////////
    
    private const float DISTANCE_TO_DAMAGE = 3.0f;
    private const float TIME_TO_DESTROY = 1.0f;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    // Pooled object states
    [Header("Destructable States")]
    [SerializeField] private GameObject NoDamageTile;
    [SerializeField] private GameObject MinorDamageTile;
    [SerializeField] private GameObject MajorDamageTile;

    [Header("Damage Effect")]
    [SerializeField] private GameObject DamageEffects;
    [SerializeField] private Transform DamageEffectsTransform;

    [Header("Destructable Properties")]
    [SerializeField] private float maxHealth = 200.0f;
    [SerializeField] private float health;

    [Range(0.0f, 1.0f)]
    [SerializeField] private float percentChanceOfDamage = 0.50f;
    [SerializeField] private float damage = 50.0f;

    // Maximum damage taken before showing minor damage state.
    // % of health taken away >=
    [Range(0.0f, 1.0f)]
    [SerializeField] private float minorMaxDamagePercent = 0.75f;

    // Maximum damage taken before showing major damage state.
    // % of health taken away >=
    [Range(0.0f, 1.0f)]
    [SerializeField] private float majorMaxDamagePercent = 1.0f;

    // private float health;
    private bool isDestroyed;

    private SpawnDestruction _SpawnDestruction;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.Init();
        this.SetupReferences();
    }

    private void Start () {
        if (this.majorMaxDamagePercent <= this.minorMaxDamagePercent) {
            Debug.LogError("Major Damage % is lower than Minor Damage % - Major Damage State will be enabled before Minor Damage State!");
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        this.health = this.maxHealth;
        this.isDestroyed = false;
    }

    private void SetupReferences () {
        this._SpawnDestruction = this.GetComponent<SpawnDestruction>();
    }

    public void DoDestruction (float multiplier, float distanceFromCenter, float radius) {
        if (!this.isDestroyed) {
            if (distanceFromCenter < DISTANCE_TO_DAMAGE) {
                if (Util.GetChance(this.percentChanceOfDamage)) {
                    this.health -= damage;
                    this.CheckDamage();
                }
            }
        }
    }

    private void CheckDamage () {
        float percentDamage = 1 - (this.health / this.maxHealth);
        if (percentDamage >= this.minorMaxDamagePercent && percentDamage < this.majorMaxDamagePercent) {
            this.ShowMinorDestruction();
        }
        if (percentDamage >= this.majorMaxDamagePercent) {
            this.ShowMajorDestruction();
            this.isDestroyed = true;
        }
    }

    private void ShowMinorDestruction () {
        this.HideNoDamageTile();
        this.HideMajorDamageTile();
        this.DoDamageEffects();
        this.ShowMinorDamageTile();
    }

    private void ShowMajorDestruction () {
        this.HideNoDamageTile();
        this.HideMinorDamageTile();
        this.DoDamageEffects();
        this.ShowMajorDamageTile();

        if (!Util.IsNull(this._SpawnDestruction)) {
            this._SpawnDestruction.SpawnRandomObjects();
        }
    }

    private void HideNoDamageTile () {
        this.NoDamageTile.SetActive(false);
    }

    private void HideMinorDamageTile () {
        this.MinorDamageTile.SetActive(false);
    }

    private void ShowMinorDamageTile () {
        this.MinorDamageTile.SetActive(true);
    }

    private void HideMajorDamageTile () {
        this.MajorDamageTile.SetActive(false);
    }

    private void ShowMajorDamageTile () {
        this.MajorDamageTile.SetActive(true);
    }

    private void DoDamageEffects () {
        GameObject effects = Instantiate(this.DamageEffects, this.DamageEffectsTransform.position, this.DamageEffectsTransform.rotation);
        Destroy(effects, TIME_TO_DESTROY);
    }

}
