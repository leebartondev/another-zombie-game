﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;


// <Studio Name>
// Lee Barton
public class WeaponsTest {

    private const float SPREAD = 0.1f;
    private const float RANGE = 100.0f;
    private const float EFFECTS_DISPLAY_TIME = 0.2f;

    // 
    [Test, Description("Test that setting HasPistol sets all other variables to false.")]
    [Author("Lee Barton")]
    public void SetPlayerHasPistol_Only_HasPistol_True () {
        Weapons.Instance.SetPlayerHasPistol();
        Assert.That(Weapons.Instance.HasPistol, Is.True);
        Assert.That(Weapons.Instance.HasRifle, Is.False);
        Assert.That(Weapons.Instance.HasThrowable, Is.False);
    }

    // 
    [Test, Description("Test that setting HasRifle sets all other variables to false.")]
    [Author("Lee Barton")]
    public void SetPlayerHasRifle_Only_HasRifle_True () {
        Weapons.Instance.SetPlayerHasRifle();
        Assert.That(Weapons.Instance.HasPistol, Is.False);
        Assert.That(Weapons.Instance.HasRifle, Is.True);
        Assert.That(Weapons.Instance.HasThrowable, Is.False);
    }

    // 
    [Test, Description("Test that setting HasLauncher sets all other variables to false.")]
    [Author("Lee Barton")]
    public void SetPlayerHasLauncher_Only_HasLauncher_True () {
        Weapons.Instance.SetPlayerHasLauncher();
        Assert.That(Weapons.Instance.HasPistol, Is.False);
        Assert.That(Weapons.Instance.HasRifle, Is.True);
        Assert.That(Weapons.Instance.HasThrowable, Is.False);
    }

    // 
    [Test, Description("Test that setting HasThrowable sets all other variables to false.")]
    [Author("Lee Barton")]
    public void SetPlayerHasThrowable_Only_HasThrowable_True () {
        Weapons.Instance.SetPlayerHasThrowable();
        Assert.That(Weapons.Instance.HasPistol, Is.False);
        Assert.That(Weapons.Instance.HasRifle, Is.False);
        Assert.That(Weapons.Instance.HasThrowable, Is.True);
    }

    // 
    [Test, Description("Test that the rate of fire conversion to timer delay is correct.")]
    [Author("Lee Barton")]
    public void RateOfFireToTimerDelay_Calculates_Timer_Delay () {
        float rateOfFire = 75.0f;

        float expectedValue = rateOfFire / Weapons.TIMER_CONVERSION;
        float actualValue = Weapons.RateOfFireToTimerDelay(rateOfFire);

        Assert.That(actualValue, Is.EqualTo(expectedValue));
    }

    // 
    /*[Test, Description("Test that the timer is equal to the set delay time.")]
    [Author("Lee Barton")]
    public void IsTimeToShoot_True_Equal () {
        float delayTimer = 0.15f;
        float delay = 0.15f;

        bool actualValue = Weapons.IsTimeToShoot(delayTimer, delay);

        Assert.That(actualValue, Is.True);
    }

    // 
    [Test, Description("Test that the timer is greater than the set delay time.")]
    [Author("Lee Barton")]
    public void IsTimeToShoot_True_Greater_Than () {
        float delayTimer = 0.20f;
        float delay = 0.15f;

        bool actualValue = Weapons.IsTimeToShoot(delayTimer, delay);

        Assert.That(actualValue, Is.True);
    }

    // 
    [Test, Description("Test that the timer is less than the set delay time.")]
    [Author("Lee Barton")]
    public void IsTimeToShoot_False () {
        float delayTimer = 0.10f;
        float delay = 0.15f;

        bool actualValue = Weapons.IsTimeToShoot(delayTimer, delay);

        Assert.That(actualValue, Is.False);
    }*/

    // 
    [Test, Description("Test that creating a weapon ray returns a ray with correct origin and direction.")]
    [Author("Lee Barton")]
    public void CreateWeaponRay_Returns_Ray () {
        GameObject obj = new GameObject();
        Vector3 muzzlePos = new Vector3(0f, 0f, 0f);

        obj.transform.forward = new Vector3(0f, 0f, 1f); 

        Ray actualRay = Weapons.CreateWeaponRay(
            SPREAD,
            RANGE,
            muzzlePos,
            obj.transform
        );

        Assert.That(actualRay.origin, Is.EqualTo(muzzlePos));
        Assert.That(
            Mathf.Approximately(Mathf.Abs(Vector3.Dot(actualRay.direction, obj.transform.forward)), 1.0f),
            Is.True
        );
    }

    //
    [Test, Description("Test that the ray from weapon does not hit the shootable mask layer.")]
    [Author("Lee Barton")]
    public void RayHitShootableMaskLayer_Does_Not_Hit () {
        GameObject obj = new GameObject();
        Vector3 muzzlePos = new Vector3(10f, 10f, 10f);
        obj.transform.forward = new Vector3(0f, 0f, 1f);

        Ray ray = Weapons.CreateWeaponRay(
            SPREAD,
            RANGE,
            muzzlePos,
            obj.transform
        );
        RaycastHit hitInfo;

        bool actualValue = Weapons.RayHitShootableMaskLayer(ray, out hitInfo, 100f);

        Assert.That(actualValue, Is.False);
    }

    //
    [Test, Description("Test that the ray from weapon hits the shootable mask layer.")]
    [Author("Lee Barton")]
    public void RayHitShootableMaskLayer_Hit () {
        GameObject obj = new GameObject();
        Vector3 muzzlePos = new Vector3(0f, 0f, 0f);
        obj.transform.forward = new Vector3(0f, 0f, 1f);

        Ray ray = Weapons.CreateWeaponRay(
            SPREAD,
            RANGE,
            muzzlePos,
            obj.transform
        );
        RaycastHit hitInfo;

        bool actualValue = Weapons.RayHitShootableMaskLayer(ray, out hitInfo, 100f);

        Assert.That(actualValue, Is.True);
    }

    //
    [Test, Description("Test that the time to display effects has passed.")]
    [Author("Lee Barton")]
    public void IsTimeToDisableEffects_True () {
        bool actual = Weapons.IsTimeToDisableEffects(
            1.0f,
            0.1f,
            EFFECTS_DISPLAY_TIME
        );

        Assert.That(actual, Is.True);
    }

    //
    [Test, Description("Test that the time to display effects has not yet passed.")]
    [Author("Lee Barton")]
    public void IsTimeToDisableEffects_False () {
        bool actual = Weapons.IsTimeToDisableEffects(
            0.0f,
            0.1f,
            EFFECTS_DISPLAY_TIME
        );

        Assert.That(actual, Is.False);
    }

    //
    [Test, Description("Test that a weapon's ammo count is equal to the infinite ammo count representation.")]
    [Author("Lee Barton")]
    public void IsInfiniteAmmo_True_When_Equal () {
        int ammo = 9999;

        bool actual = Weapons.IsInfiniteAmmo(ammo);

        Assert.That(actual, Is.True);
    }

    //
    [Test, Description("Test that a weapon's ammo count is greater than the infinite ammo count representation.")]
    [Author("Lee Barton")]
    public void IsInfiniteAmmo_True_When_Greater () {
        int ammo = 10000;

        bool actual = Weapons.IsInfiniteAmmo(ammo);

        Assert.That(actual, Is.True);
    }

    //
    [Test, Description("Test that a weapon's ammo count is not equal to or greater than the infinite ammo count representation.")]
    [Author("Lee Barton")]
    public void IsInfiniteAmmo_False () {
        int ammo = 350;

        bool actual = Weapons.IsInfiniteAmmo(ammo);

        Assert.That(actual, Is.False);
    }

    //
    [Test, Description("Test that a weapon is at full ammo.")]
    [Author("Lee Barton")]
    public void IsFullAmmo_True () {
        int ammo = 100;
        int maxAmmo = 100;

        bool actual = Weapons.IsFullAmmo(ammo, maxAmmo);

        Assert.That(actual, Is.True);
    }

    //
    [Test, Description("Test that a weapon is not at full ammo.")]
    [Author("Lee Barton")]
    public void IsFullAmmo_False () {
        int ammo = 100;
        int maxAmmo = 101;

        bool actual = Weapons.IsFullAmmo(ammo, maxAmmo);

        Assert.That(actual, Is.False);
    }

    //
    [Test, Description("Test that a weapon has ammo.")]
    [Author("Lee Barton")]
    public void IsNotEmpty_True () {
        int ammo = 1;

        bool actual = Weapons.IsNotEmpty(ammo);

        Assert.That(actual, Is.True);
    }

    //
    [Test, Description("Test that a weapon is empty.")]
    [Author("Lee Barton")]
    public void IsNotEmpty_False () {
        int ammo = 0;

        bool actual = Weapons.IsNotEmpty(ammo);

        Assert.That(actual, Is.False);
    }

    //
    [Test, Description("Test that a weapon type is not equal to the same weapon type.")]
    [Author("Lee Barton")]
    public void IsEqual_True () {
        Weapons.Enum selected = Weapons.Enum.PISTOL;
        Weapons.Enum test = Weapons.Enum.PISTOL;

        bool actual = Weapons.IsEqual(selected, test);

        Assert.That(actual, Is.True);
    }

    //
    [Test, Description("Test that a weapon type is equal to the same weapon type.")]
    [Author("Lee Barton")]
    public void IsEqual_False () {
        Weapons.Enum selected = Weapons.Enum.PISTOL;
        Weapons.Enum test = Weapons.Enum.ROCKET_LAUNCHER;

        bool actual = Weapons.IsEqual(selected, test);

        Assert.That(actual, Is.False);
    }

    //
    [Test, Description("Test that a specified weapon is able to be purchased.")]
    [Author("Lee Barton")]
    public void IsPurchasable_True () {
        Weapons.Enum weapon = Weapons.Enum.ADVANCED_PISTOL;
        int brains = 5000;

        bool actual = Weapons.IsPurchasable(weapon, brains);

        Assert.That(actual, Is.True);
    }

    //
    [Test, Description("Test that a specified weapon is not able to be purchased.")]
    [Author("Lee Barton")]
    public void IsPurchasable_False () {
        Weapons.Enum weapon = Weapons.Enum.ADVANCED_PISTOL;
        int brains = 1000;

        bool actual = Weapons.IsPurchasable(weapon, brains);

        Assert.That(actual, Is.False);
    }

    //
    [Test, Description("Test that a specified weapon has been purchased given purchased state.")]
    [Author("Lee Barton")]
    public void IsPurchased_True () {
        Weapons.Enum weapon = Weapons.Enum.ADVANCED_PISTOL;
        List<Purchasing.State> state = new List<Purchasing.State>() { Purchasing.State.PURCHASED, Purchasing.State.PURCHASED };

        bool actual = Weapons.IsPurchased(weapon, state);

        Assert.That(actual, Is.True);
}

    //
    [Test, Description("Test that a weapon has not been purchased given purchased state.")]
    [Author("Lee Barton")]
    public void IsPurchased_False () {
        Weapons.Enum weapon = Weapons.Enum.ADVANCED_PISTOL;
        List<Purchasing.State> state = new List<Purchasing.State>() { Purchasing.State.PURCHASED, Purchasing.State.LOCKED };

        bool actual = Weapons.IsPurchased(weapon, state);

        Assert.That(actual, Is.False);
    }
}
