﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

// <Studio Name>
// Lee Barton
public class UtilTests {

    // 
    [Test, Description("Test that index matches and object is not active is true.")]
    [Author("Lee Barton")]
    public void IsIndexAndNotActive_True () {
        GameObject testObj = new GameObject();
        testObj.SetActive(false);

        bool actual = Util.IsIndexAndNotActive(1, 1, testObj);

        Assert.That(actual, Is.True);
    }

    // 
    [Test, Description("Test that index matches and object is not active is false when index does not match.")]
    [Author("Lee Barton")]
    public void IsIndexAndNotActive_False_Index () {
        GameObject testObj = new GameObject();
        testObj.SetActive(false);

        bool actual = Util.IsIndexAndNotActive(1, 2, testObj);

        Assert.That(actual, Is.False);
    }

    // 
    [Test, Description("Test that index matches and object is not active is false when index does not match and object is active.")]
    [Author("Lee Barton")]
    public void IsIndexAndNotActive_False_Index_And_Object () {
        GameObject testObj = new GameObject();
        testObj.SetActive(true);

        bool actual = Util.IsIndexAndNotActive(1, 2, testObj);

        Assert.That(actual, Is.False);
    }

    // 
    [Test, Description("Test that index matches and object is not active is false when object is active.")]
    [Author("Lee Barton")]
    public void IsIndexAndNotActive_False_Object () {
        GameObject testObj = new GameObject();
        testObj.SetActive(true);

        bool actual = Util.IsIndexAndNotActive(1, 1, testObj);

        Assert.That(actual, Is.False);
    }

    // 
    [Test, Description("Test that index matches and object is active is true.")]
    [Author("Lee Barton")]
    public void IsIndexAndActive_True () {
        GameObject testObj = new GameObject();
        testObj.SetActive(true);

        bool actual = Util.IsIndexAndActive(1, 1, testObj);

        Assert.That(actual, Is.True);
    }

    // 
    [Test, Description("Test that index matches and object is active is false when index does not match.")]
    [Author("Lee Barton")]
    public void IsIndexAndActive_False_Index () {
        GameObject testObj = new GameObject();
        testObj.SetActive(true);

        bool actual = Util.IsIndexAndActive(1, 2, testObj);

        Assert.That(actual, Is.False);
    }

    // 
    [Test, Description("Test that index matches and object is active is false when index does not match and object is not active.")]
    [Author("Lee Barton")]
    public void IsIndexAndActive_False_Index_And_Object () {
        GameObject testObj = new GameObject();
        testObj.SetActive(false);

        bool actual = Util.IsIndexAndActive(1, 2, testObj);

        Assert.That(actual, Is.False);
    }

    // 
    [Test, Description("Test that index matches and object is active is false when object is not active.")]
    [Author("Lee Barton")]
    public void IsIndexAndActive_False_Object () {
        GameObject testObj = new GameObject();
        testObj.SetActive(false);

        bool actual = Util.IsIndexAndActive(1, 1, testObj);

        Assert.That(actual, Is.False);
    }
}
