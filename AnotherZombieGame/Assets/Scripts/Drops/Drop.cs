﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
// Credit: Donovan Keith (Float and Rotation Effect) v0.0.2
public class Drop : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // G L O B A L S  /  C O N S T A N T S
    /////////////////////////////////////////////////////////////////

    public static float Y_OFFSET_AT_ZERO = 0.15f;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Drop Floating Effect")]
    [SerializeField] protected bool doFloat = true;
    [Range(0.0f, 2.0f)]
    [SerializeField] protected float floatingAmplitude = 0.5f;
    [SerializeField] protected float floatingSpeed = 1.0f;
    [SerializeField] protected float yPositionOffset = 0.0f;

    [Header("Drop Rotation Effect")]
    [SerializeField] protected bool doRotation = true;
    [Range(0.0f, 180.0f)]
    [SerializeField] protected float xDegreesPerSecond = 15.0f;
    [Range(0.0f, 180.0f)]
    [SerializeField] protected float yDegreesPerSecond = 15.0f;
    [Range(0.0f, 180.0f)]
    [SerializeField] protected float zDegreesPerSecond = 15.0f;

    [Header("Drop Move To Player Effect")]
    [SerializeField] protected bool moveToPlayer = false;
    [SerializeField] protected float moveSpeed = 1.0f;
    [SerializeField] protected bool speedOverTime = false;
    [SerializeField] protected float moveSpeedInterval = 0.05f;

    private Vector3 positionOffset = new Vector3();

    private Transform playerTransform;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected virtual void Awake () {
        this.SetupReferences();
        this.Init();
    }

    protected virtual void FixedUpdate () {
        this.DoFloat();
        this.DoRotation();
        this.MoveToPlayer();
    }

    // Handle sphere collider
    protected virtual void OnTriggerEnter (Collider collider) {
        this.HandleCollision(collider);
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Setup and initialize object or script references.
    protected virtual void SetupReferences () {
        if (this.moveToPlayer) {
            GameObject player = Util.GetPlayerByTag();
            if (!Util.IsNull(player)) {
                this.playerTransform = player.transform;
            } else {
                this.moveToPlayer = false;
            }
        }
    }

    // Initialize drop veriables.
    protected virtual void Init () {
        this.transform.position = StartPosition(
            this.transform,
            this.floatingAmplitude,
            this.yPositionOffset
        );

        this.positionOffset = this.transform.position;
    }

    // The drop will float up and down for a specified range.
    protected virtual void DoFloat () {
        if (this.doFloat) {
            this.transform.position = NextFloatingPosition(
                this.transform,
                this.positionOffset,
                this.floatingSpeed,
                this.floatingAmplitude
            );
        }
    }

    // The drop will rotate on the y and z axis.
    protected virtual void DoRotation () {
        if (this.doRotation) {
            transform.Rotate(
                Util.RotationByDPS(
                    this.xDegreesPerSecond,
                    this.yDegreesPerSecond,
                    this.zDegreesPerSecond
                ),
                Space.World
            );
        }
    }

    // The drop will move towards the player over time.
    protected virtual void MoveToPlayer () {
        if (this.moveToPlayer) {
            this.gameObject.transform.position = MoveToTarget(
                this.transform.position,
                this.playerTransform.position,
                this.moveSpeed
            );

            if (this.speedOverTime) {
                this.moveSpeed += this.moveSpeedInterval;
            }
        }
    }

    protected virtual void HandleCollision (Collider collider) { }

    /////////////////////////////////////////////////////////////////
    // S T A T I C   H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Get vector step position for drop moving towards target object.
    public static Vector3 MoveToTarget (Vector3 position, Vector3 target, float speed) {
        return Vector3.MoveTowards(
            position,
            target,
            (speed * Time.deltaTime)
        );
    }

    // Get the next y position of the drop while floating.
    public static Vector3 NextFloatingPosition (Transform transform, Vector3 offset, float speed, float amplitude) {
        // PPI - instantiating Vector3 object every frame.
        return new Vector3(
            transform.position.x,
            (offset.y += (Mathf.Sin(Time.fixedTime * Mathf.PI * speed) * amplitude)),
            transform.position.z
        );
    }

    // Get the drop's initial position based on amplitude and offset in the y direction.
    public static Vector3 StartPosition (Transform transform, float amplitude, float yOffset) {
        return new Vector3(
            transform.position.x,
            amplitude + yOffset,
            transform.position.z
        );
    }

    // Spawn the specified drop around a position within a radius.
    public static GameObject HandleSpawn (GameObject drop, Transform transform, float radius) {
        return GameObject.Instantiate(
            drop,
            RandomPositionInCircle(transform.position, radius),
            transform.rotation
        );
    }

    // Spawn multiple drop objects around a position within a radius.
    public static GameObject[] HandleSpawn (GameObject drop, Transform transform, float radius, int count) {
        GameObject[] drops = new GameObject[count];
        for (int i = 0; i < count; i++) {
            drops[i] = HandleSpawn(drop, transform, radius);
        }
        return drops;
    }

    // Return a random vector within a circle around a specified position.
    public static Vector3 RandomPositionInCircle (Vector3 from, float radius) {
        Vector3 rand = (((Vector3)Random.insideUnitCircle * radius) + from);
        rand.y = 0f;
        return rand;
    }

    // Check that a drop can spawn based on chance.
    public static bool DropCanSpawn (float chance) {
        return (Random.Range(0.0f, 1.0f) <= chance);
    }

    // Get the amount of drop objects to spawn.
    public static int GetSpawnCount (int min, int max) {
        if (min != max) {
            return Random.Range(min, (max + 1));
        }
        return min;
    }
}
