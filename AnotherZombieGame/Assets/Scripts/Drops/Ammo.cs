﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class Ammo : Drop {

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Setup brains object.
    protected override void Init () {
        base.Init();
    }

    // Handle player collision with ammo.
    protected override void HandleCollision (Collider collider) {
        GameObject collided = collider.gameObject;
        if (collided.tag == Tag.PLAYER) {
            WeaponsContainer _WeaponsContainer = collided.GetComponentInChildren<WeaponsContainer>();
            this.UseAmmo(_WeaponsContainer);
        }
    }

    protected virtual void UseAmmo (WeaponsContainer _WeaponsContainer) {
        bool reloadedWeapon = _WeaponsContainer.ReloadWeapon();
        bool reloadedGrenade = _WeaponsContainer.ReloadGrenade();

        if (reloadedWeapon || reloadedGrenade) {
            // @TODO pickup sound effect
            Destroy(this.gameObject);
        }
    }
}
