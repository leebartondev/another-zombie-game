﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class DropHandler : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    #pragma warning disable 0649
    [SerializeField] private GameObject dropItem;

    [SerializeField] private int minimum = 1;
    [SerializeField] private int maximum = 1;
    [SerializeField] private float chanceOfDrop = 1.0f;

    [SerializeField] private float dropRadius = 2.0f;

    private EnemyHealth _EnemyHealth;
    private bool dropped;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Start is called before the first frame update
    private void Awake () {
        this.Init();
        this.SetupReferences();
    }

    private void LateUpdate () {
        if (_EnemyHealth.IsDead() && !this.dropped) {
            this.DoDrop();
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        this.dropped = false;
    }

    private void SetupReferences () {
        this._EnemyHealth = this.gameObject.GetComponent<EnemyHealth>();
    }

    // Handle dropping items.
    private void DoDrop () {
        if (Drop.DropCanSpawn(this.chanceOfDrop)) {
            int count = Drop.GetSpawnCount(this.minimum, this.maximum);

            for (int i = 0; i < count; i++) {
                Drop.HandleSpawn(
                    this.dropItem,
                    this.transform,
                    this.dropRadius
                );
            }
        }
        this.dropped = true;
    }
}
