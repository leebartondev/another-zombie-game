﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class CameraInteraction : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // G L O B A L S  /  C O N S T A N T S
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private float zoomSpeed = 5.0f;

    private Vector3 positionOffset;
    private Quaternion rotationOffset;

    private Vector3 targetPosition;
    private Quaternion targetRotation;

    private bool zoomIn;

    private Transform playerTransform;

    private CameraFollow _CameraFollow;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.Init();
        this.SetupReferences();
    }

    private void LateUpdate () {
        Zoom();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        this.positionOffset = this.transform.position;
        this.rotationOffset = this.transform.rotation;
    }

    private void SetupReferences () {
        GameObject player = Util.GetPlayerByTag();
        if (!Util.IsNull(player)) {
            this.playerTransform = Util.GetPlayerByTag().transform;
        }
        this._CameraFollow = this.gameObject.GetComponent<CameraFollow>();
    }

    private void Zoom () {
        if (this.zoomIn) {
            float time = Time.deltaTime * this.zoomSpeed;

            this.transform.position = Vector3.Lerp(
                this.transform.position,
                this.targetPosition,
                time
            );
            this.transform.rotation = Quaternion.Lerp(
                this.transform.rotation,
                this.targetRotation,
                time
            );

            bool reachedTargetPosition = this.transform.position == this.targetPosition;
            bool reachedTargetRotation = this.transform.rotation == this.targetRotation;
            if (reachedTargetPosition && reachedTargetRotation) {
                this.zoomIn = false;
            }
        }
    }

    // Prepare for camera to zoom in to interactable.
    public void DoCameraZoomIn (GameObject interactable, Transform transform) {
        this.targetPosition = transform.position;
        this.targetRotation = transform.rotation;

        this._CameraFollow.enabled = false;
        this.transform.SetParent(interactable.transform);

        this.zoomIn = true;
    }

    // Prepare for camera to zoom out to original position.
    public void ResetCamera () {
        this.zoomIn = false;
        
        this.transform.SetParent(null);
        this.transform.position = (this.playerTransform.position + this.positionOffset);
        this.transform.rotation = this.rotationOffset;

        this._CameraFollow.enabled = true;
    }
}
