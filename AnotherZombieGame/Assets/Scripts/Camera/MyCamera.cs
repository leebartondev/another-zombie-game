﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// <Studio Name>
// Lee Barton
// Using Singleton Pattern
public sealed class MyCamera {

    /////////////////////////////////////////////////////////////////
    // I N S T A N C E
    /////////////////////////////////////////////////////////////////

    private static readonly MyCamera instance = new MyCamera();

    // Explicit static constructor to tell C# compiler to not mark
    // type as 'beforefieldinit'
    static MyCamera () {}
    private MyCamera () {}

    public static MyCamera Instance {
        get {
            return instance;
        }
    }

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////
    
    public bool Smoothing { get; set; }
    public float SmoothingMultiplier { get; set; }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Calculate position the camera should move to.
    public Vector3 CalculateNewPosition (Vector3 cameraPos, Vector3 targetPos, Vector3 offset) {
        Vector3 aimPos = targetPos + offset;

        if (this.Smoothing) {
            return Vector3.Lerp(
                cameraPos,
                aimPos,
                (this.SmoothingMultiplier * Time.deltaTime)
            );
        } else {
            return aimPos;
        }
    }

    // Update camera smoothing based on Unity interface.
    public void UpdateSmoothing (bool smoothing) {
        if (this.Smoothing != smoothing) {
            this.Smoothing = smoothing;
        }
    }

    // Update camera smoothing multiplier based on Unity interface.
    public void UpdateSmoothingMultiplier (float multiplier) {
        if (this.SmoothingMultiplier != multiplier) {
            this.SmoothingMultiplier = multiplier;
        }
    }
}
