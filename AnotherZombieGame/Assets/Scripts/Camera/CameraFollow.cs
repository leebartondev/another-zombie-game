﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class CameraFollow : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private Transform target;

    [Header("Smoothing Effect")]
    [SerializeField] private bool smoothing = true;
    [SerializeField] private float smoothingMultiplier = 5f;

    private Vector3 cameraOffset; // Position the camera starts in

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        if (Util.IsNull(target)) {
            GameObject player = Util.GetPlayerByTag();

            if (Util.IsNull(player)) {
                Debug.LogError("CameraFollow :: Awake() :: Unable to set camera target, player not found!");
                return;
            }

            if (Util.IsNull(player.transform)) {
                Debug.LogError("CameraFollow :: Awake() :: Unable to set camera target, player transform not found!");
                return;
            }

            target = player.transform;
        }
    }

    private void Start () {
        MyCamera.Instance.Smoothing = this.smoothing;
        MyCamera.Instance.SmoothingMultiplier = this.smoothingMultiplier;

        if (!Util.IsNull(this.target)) {
            this.cameraOffset = this.transform.position - this.target.position;
        }
    }

    private void FixedUpdate () {
        MyCamera.Instance.UpdateSmoothing(this.smoothing);
        MyCamera.Instance.UpdateSmoothingMultiplier(this.smoothingMultiplier);

        FollowTarget();
    }

	/////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Camera follows the player.
    private void FollowTarget () {
        if (!Util.IsNull(this.target)) {
            this.transform.position = MyCamera.Instance.CalculateNewPosition(
                this.transform.position,
                this.target.position,
                this.cameraOffset
            );
        }
    }
}
