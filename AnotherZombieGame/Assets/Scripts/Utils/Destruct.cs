using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class Destruct : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    // Pooled object states
    [Header("Object States")]
    [SerializeField] protected GameObject DefaultVersion;
    [SerializeField] protected GameObject BrokenVersion;

    [Header("Damage Effects")]
    [SerializeField] protected GameObject DamageEffects;
    [SerializeField] protected GameObject BreakEffects;

    [Header("Cleanup Settings")]
    [SerializeField] protected bool destroy = true;
    [SerializeField] protected float destroyDelay = 5.0f;

    protected bool isDestroyed;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.Init();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected virtual void Init () {
        this.BrokenVersion.SetActive(false);
        this.DefaultVersion.SetActive(true);
    }

    public virtual void DoBreak () {
        this.isDestroyed = true;
        this.DoBreakEffects();
        this.DefaultVersion.SetActive(false);
        this.BrokenVersion.SetActive(true);
        
        if (this.destroy) {
            Destroy(this.gameObject, this.destroyDelay);
        }
    }

    protected virtual void DoDamageEffects () {
        if (!Util.IsNull(this.DamageEffects)) {
            // Instantiate effects
            Debug.LogWarning("Unimplemented!");
        }
    }

    protected virtual void DoBreakEffects () {
        if (!Util.IsNull(this.BreakEffects)) {
            // Instantiate effects
            Debug.LogWarning("Unimplemented!");
        }
    }
}
