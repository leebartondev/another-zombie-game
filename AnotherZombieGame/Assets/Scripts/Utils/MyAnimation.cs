﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public static class MyAnimation {
    // @PPI Replace strings with int values for animator IDs.

    /////////////////////////////////////////////////////////////////
    // C O N T R O L L E R   L A Y E R S
    /////////////////////////////////////////////////////////////////

    // Layers
    public static string BASE_LAYER = "Base Layer";

    public static string RIFLE_LAYER = "Rifle Layer";
    public static string THROWABLE_LAYER = "Throwable Layer";
    public static string LAUNCHABLE_LAYER = "Launchable Layer";

    public static string ATTACK_LAYER = "Attack Layer";

    /////////////////////////////////////////////////////////////////
    // C O N T R O L L E R   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    // Movement
    public static string IS_WALKING = "IsWalking";
    public static string IS_WALKING_BACK = "IsWalkingBack";
    public static string IS_STRAFING_LEFT = "IsStrafingLeft";
    public static string IS_STRAFING_RIGHT = "IsStrafingRight";
    public static string IS_THROWING = "IsThrowing";

    public static string IS_DEAD = "IsDead";

    public static string IS_SPRINTING = "IsSprinting";
    public static string IS_SLOW = "IsSlow";

    public static string IS_ATTACKING = "IsAttacking";
    public static string HAS_TARGET = "HasTarget";

    // Triggers
    public static string STOMP_ATTACK = "StompAttack";
    public static string SPAWN_COMPLETE = "SpawnComplete";
    public static string SPAWN_START = "SpawnStart";
    public static string NEW_ROUND = "NewRound";

    // Type
    public static string HAS_PISTOL = "HasPistol";
    public static string HAS_RIFLE = "HasRifle";
    public static string HAS_THROWABLE = "HasThrowable";
    public static string HAS_LAUNCHABLE = "HasLaunchable";
}
