﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Novasloth Games LLC
// Lee Barton
public static class Util { //@TEST

    /////////////////////////////////////////////////////////////////
    // S T A T I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public static float MARGIN_OF_ERROR = 0.001f;

    /////////////////////////////////////////////////////////////////
    // U T I L I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // @TODO organize methods

    //
    // MATHS & PHYSICS //
    //

    // Return true or false if random value is less than or equal to chance.
    public static bool GetChance (float chance) => (UnityEngine.Random.Range(0.0f, 1.0f) <= chance);

    // Check that timer is at or past delay
    public static bool TimerCheck (float timer, float delay) => (timer >= delay);

    // Calculate damage multiplier from 0 to 1 based on distance to max distance.
    public static float CalculateExplosionDamageMultiplier (float maxDistance, float distanceTo) {
        return (((-1 / maxDistance) * distanceTo) + 1);
    }

    //
    // TYPE CHECKS, SEARCH //
    //

    // Compare gameobjects
    public static bool IsEqual (GameObject gameObject1, GameObject gameObject2)
        => gameObject1?.GetInstanceID() == gameObject2?.GetInstanceID();

    // Check for player tag.
    public static bool IsPlayer (GameObject gameObject) => (gameObject.tag == Tag.PLAYER);
    public static bool IsPlayer (Collider collider) => IsPlayer(collider.gameObject);

    // Get player gameobject by tag.
    public static GameObject GetPlayerByTag () => GameObject.FindGameObjectWithTag(Tag.PLAYER);

    // Get HUD gameobject by tag.
    public static GameObject GetHUDByTag () => GameObject.FindGameObjectWithTag(Tag.HUD);

    // Get main camera gameobject by tag.
    public static GameObject GetMainCameraByTag () => GameObject.FindGameObjectWithTag(Tag.MAIN_CAMERA);

    // Get spawner gameobject by tag.
    public static GameObject GetSpawnerByTag () => GameObject.FindGameObjectWithTag(Tag.SPAWNER);

    // Get round system gameobject by tag.
    public static GameObject GetRoundSystemByTag () => GameObject.FindGameObjectWithTag(Tag.ROUND_SYSTEM);

    // Get specific parent by tag recursively.
    public static Transform GetParentByTag (Transform startingChild, string tag, int currentIndex, int maxIndex) {
        Transform parentOfChild = startingChild.parent;

        // Handle parent
        if (parentOfChild == null) {
            return null;
        } else if (parentOfChild.tag == tag) {
            return parentOfChild;
        }

        // Handle index
        if (currentIndex < maxIndex) { // maxIndex 0 = go to root
            currentIndex++;
        } else {
            return null;
        }

        return GetParentByTag(parentOfChild, tag, currentIndex, maxIndex);
    }

    // Test that children exist in a given transform and return the first child or null.
    public static Transform TryAndGetFirstChild (Transform transform) {
        if (transform.childCount > 0) {
            return transform.GetChild(0);
        }

        return null;
    }

    // Check for specific index and an object is active in the hierarchy.
    public static bool IsIndexAndNotActive (int index, int match, GameObject obj) {
        return (index == match && !obj.activeInHierarchy);
    }

    // Check for specific index and an object is active in the hierarchy.
    public static bool IsIndexAndActive (int index, int match, GameObject obj) {
        return (index == match && obj.activeInHierarchy);
    }

    // Check if Unity object is null or behaving like it should be null.
    public static bool IsNull (System.Object obj) {
        return obj == null || obj.Equals(null);
    }

    // Get vector rotation for transform on x, y, and z axis by degrees per second.
    public static Vector3 RotationByDPS (float xDPS, float yDPS, float zDPS) {
        // PPI - instantiating Vector3 object every frame.
        return new Vector3(
            (Time.deltaTime * xDPS),
            (Time.deltaTime * yDPS),
            (Time.deltaTime * zDPS)
        );
    }

    public static bool Vector3Equal (Vector3 vectorA, Vector3 vectorB) {
        return Vector3.SqrMagnitude(vectorA - vectorB) < 0.0001;
    }

    public static bool ReachedTarget (Vector3 targetVector, Vector3 movingVector) {
        return Vector3.Distance(targetVector, movingVector) < MARGIN_OF_ERROR;
    }

    //
    // NAV AGENT //
    //

    // Reliably check if the provided NavMeshAgent has reached its destination.
    public static bool NavAgentReachedDestination (NavMeshAgent navAgent, float distance) {
        if (!navAgent.pathPending) { // Hasn't started calculating path to destination yet.
            if (navAgent.remainingDistance <= navAgent.stoppingDistance) { // Are we within stopping distance?
                if (!navAgent.hasPath || navAgent.velocity.sqrMagnitude <= distance) { // We no longer have a path to a destination & not moving
                    return true;
                }
            }
        }

        return false;
    }

    // Check that nav agent has reached it's target position.
    public static bool NavAgentReachedDestination (NavMeshAgent navAgent) {
        return NavAgentReachedDestination(navAgent, 0.0f);
    }

    //
    // --- //
    //

    // Enum enumerable helper method
    public static IEnumerable<T> GetEnumValues<T> () {
        return Enum.GetValues(typeof(T)).Cast<T>();
    }

    /////////////////////////////////////////////////////////////////
    // T Y P E   S W I T C H   L O O K U P
    /////////////////////////////////////////////////////////////////

    // https://stackoverflow.com/a/10025398
    // https://gist.github.com/Virtlink/8722649
    public static class TypeSwitch {
        public static Switch<TSource> On<TSource> (TSource value) {
            return new Switch<TSource>(value);
        }

        public sealed class Switch<TSource> {
            private readonly TSource value;
            private bool handled = false;

            internal Switch (TSource value) {
                this.value = value;
            }

            public Switch<TSource> Case<TTarget> (Action<TTarget> action)
                where TTarget : TSource {

                if (!this.handled && this.value is TTarget) {
                    action((TTarget) this.value);
                    this.handled = true;
                }

                return this;
            }

            public void Default (Action<TSource> action) {
                if (!this.handled) {
                    action(this.value);
                }
            }
        }
    }
}
