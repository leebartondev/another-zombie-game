using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// Lee Barton
public class PerkMachine : Interactable, IEventRegisterable {

    private const string PRICE_LOCKED_TEXT = "NOT ENOUGH BRAINS";

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Display")]
    [SerializeField] private Transform perkModels;
    [SerializeField] private Transform perkMachineButtons;
    [SerializeField] private GameObject[] perkBuyParticleEffects;
    [SerializeField] private List<Material> lockedMaterialsList;
    [SerializeField] private List<Material> purchasableMaterialsList;
    [Range(-180.0f, 180.0f)]
    [SerializeField] private float yRotationPerSecond = 20.0f;

    [Header("UI")]
    [SerializeField] private TextMeshProUGUI[] priceTextList;
    [SerializeField] private GameObject[] hoverButtonList;

    private PlayerStats _PlayerStats;

    private Perk[] perksList;
    private Dictionary<Perk.Enum, Perk> perkLookup = new Dictionary<Perk.Enum, Perk>();

    // Display
    private Dictionary<Perk.Enum, Material> perkMaterialLookup = new Dictionary<Perk.Enum, Material>();
    private Transform perkModelFocused;

    // UI
    private Dictionary<Perk.Enum, TextMeshProUGUI> priceTextLookup = new Dictionary<Perk.Enum, TextMeshProUGUI>();

    // Colliders
    private BoxCollider boxCollider;
    private SphereCollider sphereCollider;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Awake () {
        base.Awake();
        this.SetupReferences();
        this.Init();
        this.RegisterEventHandlers();
    }

    protected override void Update () {
        base.Update();
    }

    private void LateUpdate () {
        if (!Util.IsNull(this.perkModelFocused)) {
            this.RotatePerkDisplayModel(this.perkModelFocused);
        }
    }

    private void OnDisable () {
        if (this.IsInteracting) {
            this.ToggleInteraction();
            this.ToggleText(false);
        }

        this.boxCollider.enabled = false;
        this.sphereCollider.enabled = false;
    }

    private void OnEnable () {
        this.boxCollider.enabled = true;
        this.sphereCollider.enabled = true;
    }

    private void OnDestroy () {
        this.UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void SetupReferences () {
        GameObject player = Util.GetPlayerByTag();
        this._PlayerStats = player.GetComponent<PlayerStats>();
        this.perksList = player.GetComponents<Perk>();

        this.boxCollider = this.GetComponent<BoxCollider>();
        this.sphereCollider = this.GetComponent<SphereCollider>();
    }

    private void Init () {
        // Hide all perk display models
        foreach (Transform perkModel in this.perkModels) {
            this.HidePerkDisplayModel(perkModel);
        }

        // Initailize dictionaries & perk display models
        int brains = this._PlayerStats.Brains;
        Perk perk = null;
        for (int i = 0; i < this.perksList.Length; i++) {
            // Initialize perk dictionary based on Perk.Enum
            perk = this.perksList[i];
            this.perkLookup.Add(perk.PerkType, perk);
            this.priceTextLookup.Add(perk.PerkType, this.priceTextList[(int)perk.PerkType]);

            // Initialize perk purchasable material based on Perk.Enum
            if (this.perksList.Length <= this.purchasableMaterialsList.Count) {
                this.perkMaterialLookup.Add(perk.PerkType, this.purchasableMaterialsList[(int)perk.PerkType]);
            } else {
                Debug.LogError($"ERROR :: PerkMachine.Init() :: Not enough materials for perks!");
            }

            // Initialize perk model display
            this.UpdatePerkDisplayModel(perk, brains);
            this.HidePerkPriceText(perk.PerkType);
        }

        this.ToggleHoverButtons(false);
    }

    protected override void ToggleInteraction () {
        base.ToggleInteraction();
        this.ToggleHoverButtons(this.IsInteracting);
    }

    // Check if perk is purchased, purchasable, or locked.
    private void UpdatePerkDisplayModel (Perk perk, int brains) {
        int perkIndex = (int)perk.PerkType;
        Transform perkModel = this.perkModels.GetChild(perkIndex);
        Transform perkMachineButton = this.perkMachineButtons.GetChild(perkIndex);

        this.ShowPerkDisplayModel(perkModel);
        this.ResetPerkMachineButton(perkMachineButton);

        if (perk.IsActive) { // Purchased
            this.HidePerkDisplayModel(perkModel);
            this.PushPerkMachineButton(perkMachineButton);
            this.ToggleHoverButton(perk.PerkType, false);
            this.HidePerkPriceText(perk.PerkType);
        } else {
            this.CheckPerkPurchasable(perk, perkModel, brains);
        }
    }

    private void UpdatePerkDisplayModel (Perk perk) {
        this.UpdatePerkDisplayModel(perk, this._PlayerStats.Brains);
    }

    //
    private void CheckPerkPurchasable (Perk perk, Transform perkModel, int brains) {
        bool perkIsPurchasable = !perk.IsActive && Perk.IsPurchasable(perk, brains);
        Renderer perkModelRenderer = perkModel.GetComponentInChildren<Renderer>();

        if (perkIsPurchasable) { // Purchasable
            Material perkMaterial;
            if (this.perkMaterialLookup.TryGetValue(perk.PerkType, out perkMaterial)) {
                perkModelRenderer.material = perkMaterial;
                // Set price text
                this.SetPerkPriceText(perk.PerkType, $"${perk.Price}");
            } else {
                Debug.LogError($"ERROR :: PerkMachine.HandlePerkDisplayOnPurchasable() :: Unable to find perk ${perk.PerkType} purchasable material!");
            }
        } else if (!perk.IsActive) { // Locked
            perkModelRenderer.material = this.lockedMaterialsList[0];
            // Set price text
            this.SetPerkPriceText(perk.PerkType, PRICE_LOCKED_TEXT, "#B70B0B");
        }

        this.CheckAndEnableHoverButton(perk.PerkType, !perk.IsActive);
    }

    private void CheckPerkPurchasable (Perk perk, int brains) {
        Transform perkModel = this.perkModels.GetChild((int)perk.PerkType);
        this.CheckPerkPurchasable(perk, perkModel, brains);
    }

    private void HidePerkDisplayModel (Transform perkModel) {
        perkModel.gameObject.SetActive(false);
    }

    private void ShowPerkDisplayModel (Transform perkModel) {
        perkModel.gameObject.SetActive(true);
    }

    private void PushPerkMachineButton (Transform perkMachineButton) {
        perkMachineButton.localPosition = new Vector3( // @PPI
            perkMachineButton.localPosition.x,
            perkMachineButton.localPosition.y,
            0.03f
        );
    }

    private void ResetPerkMachineButton (Transform perkMachineButton) {
        perkMachineButton.localPosition = new Vector3( // @PPI
            perkMachineButton.localPosition.x,
            perkMachineButton.localPosition.y,
            0.0f
        );
    }

    private void RotatePerkDisplayModel (Transform perkModel) {
        float yRotation = this.perkModelFocused.rotation.eulerAngles.y;

        if (yRotation > 45.0f && yRotation < 315.0f) {

            this.yRotationPerSecond *= -1;
        }

        perkModel.Rotate(
            Util.RotationByDPS(
                0,
                this.yRotationPerSecond,
                0
            ),
            Space.World
        );
    }

    public void PurchasePerk (Perk.Enum perkType) {
        Perk perk;
        if (this.perkLookup.TryGetValue(perkType, out perk)) {
            if (!perk.IsActive) {
                if (this._PlayerStats.TryUseBrains(perk.Price)) {
                    perk.Purchased();
                    PurchasingEventHandler.PerkPurchased(perk.PerkType);
                    DoPerkPurchasedParticleEffects((int)perk.PerkType);
                }
            } else {
                Debug.Log($"INFO :: PerkMachine.PurchasePerk() :: {perkType} Perk already purchased!");
            }
        }
    }

    public void PurchasePerk (int perkEnumIndex) {
        this.PurchasePerk((Perk.Enum)perkEnumIndex);
    }

    private void DoPerkPurchasedParticleEffects (int perkEnumIndex) {
        GameObject particleEffect = this.perkBuyParticleEffects[perkEnumIndex];
        if (!Util.IsNull(particleEffect)) {
            Transform perkModel = this.perkModels.GetChild(perkEnumIndex);
            GameObject clonedParticleEffect = Instantiate(
                particleEffect,
                perkModel.position,
                particleEffect.transform.rotation
            );
            Destroy(clonedParticleEffect, 1.0f);
        }
    }

    private void SetPerkPriceText (Perk.Enum perk, string text, string hexColor) {
        TextMeshProUGUI priceText;
        if (priceTextLookup.TryGetValue(perk, out priceText)) {
            priceText.text = text;

            Color color;
            if (ColorUtility.TryParseHtmlString(hexColor, out color)) {
                priceText.color = color;
            } else {
                Debug.LogError($"ERROR :: PerkMachine.SetPerkPriceText() :: Could not set hex color {hexColor}!");
            }
        }
    }

    private void SetPerkPriceText (Perk.Enum perk, string text) {
        this.SetPerkPriceText(perk, text, "#FFFFFF");
    }

    /////////////////////////////////////////////////////////////////
    // H O V E R   B U T T O N S
    /////////////////////////////////////////////////////////////////

    //
    public void HidePerkPriceText (Perk.Enum perkType) {
        TextMeshProUGUI priceText;
        if (priceTextLookup.TryGetValue(perkType, out priceText)) {
            priceText.gameObject.SetActive(false);
        }
    }

    public void HidePerkPriceText (int index) {
        this.HidePerkPriceText((Perk.Enum)index);
    }

    //
    public void ShowPerkPriceText (Perk.Enum perkType) {
        TextMeshProUGUI priceText;
        if (priceTextLookup.TryGetValue(perkType, out priceText)) {
            priceText.gameObject.SetActive(true);
        }
    }

    //
    public void ShowPerkPriceText (int index) {
        this.ShowPerkPriceText((Perk.Enum)index);
    }

    public void SetFocusedPerkDisplayModel (int index) {
        this.perkModelFocused = this.perkModels.GetChild(index);
    }

    public void RemoveFocusedPerkDisplayMode () {
        this.perkModelFocused = null;
    }

    //
    private void ToggleHoverButtons (bool toggle) {
        for (int i = 0; i < hoverButtonList.Length; i++) {
            if (perkModels.GetChild(i).gameObject.activeInHierarchy) {
                hoverButtonList[i].SetActive(toggle);
            }
        }
    }

    private void ToggleHoverButton (int index, bool toggle) {
        hoverButtonList[index].SetActive(toggle);
    }

    private void ToggleHoverButton (Perk.Enum perkType, bool toggle) {
        this.ToggleHoverButton((int)perkType, toggle);
    }

    private void CheckAndEnableHoverButton (Perk.Enum perkType, bool perkDisabled) {
        GameObject hoverButton = this.hoverButtonList[(int)perkType];

        if (this.IsInteracting && perkDisabled && !hoverButton.activeInHierarchy) {
            hoverButton.SetActive(true);
        }
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        PlayerStatsEventHandler.PlayerStatsBrainsUpdate +=
            this.OnPlayerStatsBrainsUpdated;
        PerkEventHandler.PerkDeactivated +=
            this.OnPerkEvent;
        PerkEventHandler.PerkActivated +=
            this.OnPerkEvent;
    }

    public void UnregisterEventHandlers () {
        PlayerStatsEventHandler.PlayerStatsBrainsUpdate -=
            this.OnPlayerStatsBrainsUpdated;
        PerkEventHandler.PerkDeactivated -=
            this.OnPerkEvent;
        PerkEventHandler.PerkActivated -=
            this.OnPerkEvent;
    }

    private void OnPlayerStatsBrainsUpdated (int brains) {
        foreach (Perk perk in this.perksList) {
            this.CheckPerkPurchasable(perk, brains);
        }
    }

    // Update perk display model
    private void OnPerkEvent (Perk perk) {
        this.UpdatePerkDisplayModel(perk);
    }
}
