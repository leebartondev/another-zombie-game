
// Lee Barton
namespace Novasloth.SpiderBoss {
    public class SpitAttack : IState {

        /////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        /////////////////////////////////////////////////////////////////

        private readonly SpiderBossAI _SpiderBossAI;

        /////////////////////////////////////////////////////////////////
        // C O N S T R U C T O R
        /////////////////////////////////////////////////////////////////

        public SpitAttack (SpiderBossAI spiderBossAI) {
            this._SpiderBossAI = spiderBossAI;
        }

        /////////////////////////////////////////////////////////////////
        // I S T A T E   M E T H O D S
        /////////////////////////////////////////////////////////////////

        public void OnEnter () {
            this._SpiderBossAI.StartSpitAttack();
        }

        public void Tick () {
            this._SpiderBossAI.LookAtPlayer();

        }

        public void OnExit () {
            this._SpiderBossAI.StopSpitAttack();
        }
    }
}