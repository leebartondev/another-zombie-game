
// Lee Barton
// @TODO remove unused variables & methods
namespace Novasloth.ZombieBoss {
    public class Spawn : IState {

        /////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        /////////////////////////////////////////////////////////////////

        private readonly ZombieBossAI _ZombieBossAI;

        /////////////////////////////////////////////////////////////////
        // C O N S T R U C T O R
        /////////////////////////////////////////////////////////////////

        public Spawn (ZombieBossAI zombieBossAI) {
            this._ZombieBossAI = zombieBossAI;
        }

        /////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        /////////////////////////////////////////////////////////////////

        public void OnEnter () {
            this._ZombieBossAI.StartSpawn();
        }

        public void Tick () {}

        public void OnExit () {
            this._ZombieBossAI.StopSpawn();
        }
    }
}
