using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton

namespace Novasloth.ZombieBoss {
    public class MoveToPlayer : IState {

        /////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        /////////////////////////////////////////////////////////////////

        private readonly ZombieBossAI _ZombieBossAI;

        /////////////////////////////////////////////////////////////////
        // C O N S T R U C T O R
        /////////////////////////////////////////////////////////////////

        public MoveToPlayer (ZombieBossAI zombieBossAI) {
            this._ZombieBossAI = zombieBossAI;
        }

        /////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        /////////////////////////////////////////////////////////////////

        public void OnEnter () {
            this._ZombieBossAI.StartMovingToPlayer();
        }

        public void Tick () {
            this._ZombieBossAI.MoveToPlayer();
        }

        public void OnExit () {
            this._ZombieBossAI.StopMovingToPlayer();
        }

    }
}
