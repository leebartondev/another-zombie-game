﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

public class DevelopmentSceneTests {

    private const string TAG_MAIN_CAMERA = "MainCamera";
    private const string TAG_GROUND_MASK = "GroundMask";

    private const int LAYER_GROUND = 9;

    [SetUp]
    public void Init () {
        SceneManager.LoadScene("Dev", LoadSceneMode.Single); // Load "Dev" Scene
    }

    //
    [UnityTest, Description("Test that the 'dev' scene loads with expected name.")]
    [Author("Lee Barton")]
    public IEnumerator Development_Scene_Loads () {
        string expectedSceneName = "Dev";

        yield return null;
        string actualSceneName = SceneManager.GetActiveScene().name;

        Assert.That(actualSceneName, Is.EqualTo(expectedSceneName));
    }

    //
    [UnityTest, Description("Test that the 'dev' scene contains a main camera object named 'Main Camera' with a Camera script.")]
    [Author("Lee Barton")]
    public IEnumerator Development_Scene_Has_Main_Camera () {
        string expectedName = "Main Camera";
        yield return null;

        GameObject MainCameraObject = GameObject.FindGameObjectWithTag(TAG_MAIN_CAMERA);
        Camera cameraComponent = MainCameraObject.GetComponent<Camera>();
        string actualName = MainCameraObject.name;

        Assert.That(actualName, Is.EqualTo(expectedName));
        Assert.That(cameraComponent, Is.Not.Null);
    }

    //
    [UnityTest, Description("Test that the 'dev' scene contains an object at the center of the world representing the map the player moves around in.")]
    [Author("Lee Barton")]
    public IEnumerator Development_Scene_Has_Ground () {
        string expectedName = "Map";
        Vector3 expectedPos = new Vector3(0f, 0f, 0f);
        yield return null;

        GameObject GroundObject = GameObject.Find("Map");
        Vector3 actualPos = GroundObject.transform.position;
        string actualName = GroundObject.name;

        Assert.That(actualName, Is.EqualTo(expectedName));
        Assert.That(actualPos, Is.EqualTo(expectedPos));
    }

    //
    [UnityTest, Description("Test that the 'dev' scene contains a ground mask on the 'Ground' layer for raycasting from the camera.")]
    [Author("Lee Barton")]
    public IEnumerator Development_Scene_Has_Ground_Mask () {
        string expectedName = "GroundMask";
        int expectedLayer = LAYER_GROUND;
        Vector3 expectedPos = new Vector3(0f, 0f, 0f);
        yield return null;

        GameObject GroundMaskObject = GameObject.FindGameObjectWithTag(TAG_GROUND_MASK);
        Vector3 actualPos = GroundMaskObject.transform.position;
        string actualName = GroundMaskObject.name;
        int actualLayer = GroundMaskObject.layer;

        Assert.That(actualName, Is.EqualTo(expectedName));
        Assert.That(actualLayer, Is.EqualTo(expectedLayer));
        Assert.That(actualPos, Is.EqualTo(expectedPos));
    }
}
