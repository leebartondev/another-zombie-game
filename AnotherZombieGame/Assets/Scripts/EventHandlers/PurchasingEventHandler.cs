﻿using System;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
// @TODO refactor: rename Actions with "...Event"
public class PurchasingEventHandler : MonoBehaviour {

    // Setup, Util
    public static event Action PurchasingSetupCompleteEvent;

    // Weapons
    public static event Action<Weapons.Enum> WeaponPurchasingStateLockedEvent;
    public static event Action<Weapons.Enum> WeaponPurchasingStatePurchasableEvent;
    public static event Action<Weapons.Enum> WeaponPurchasingStatePurchasedEvent;

    public static event Action<Weapons.Enum, Purchasing.State> WeaponPurchasingStateUpdatedEvent;

    // Perks
    public static event Action<Perk.Enum> PerkPurchasedEvent;

    // Upgrades
    public static event Action<Weapons.Enum, WeaponUpgrade.Enum> UpgradePurchasingStateLockedEvent;
    public static event Action<Weapons.Enum, WeaponUpgrade.Enum> UpgradePurchasingStatePurchasableEvent;
    public static event Action<Weapons.Enum, WeaponUpgrade.Enum> UpgradePurchasingStatePurchasedEvent;

    public static event Action<Weapons.Enum, WeaponUpgrade.Enum, Purchasing.State> UpgradePurchasingStateUpdatedEvent;

    /////////////////////////////////////////////////////////////////
    // S E T U P   E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public static void PurchasingSetupComplete () {
        // Debug.Log("INFO :: PurchasingEventHandler :: PurchasingSetupComplete()");
        PurchasingSetupCompleteEvent?.Invoke();
    }

    /////////////////////////////////////////////////////////////////
    // W E A P O N   E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    // Handle weapon purchasing state updated.
    public static void WeaponPurchasingStateUpdated (Weapons.Enum weapon, Purchasing.State state) {
        WeaponPurchasingStateUpdatedEvent?.Invoke(weapon, state);
    }

    // Handle weapon locked state.
    public static void WeaponLocked (Weapons.Enum weapon) {
        WeaponPurchasingStateLockedEvent?.Invoke(weapon);
    }

    // Handle weapon purchasable state.
    public static void WeaponPurchasable (Weapons.Enum weapon) {
        WeaponPurchasingStatePurchasableEvent?.Invoke(weapon);
    }

    // Handle weapon purchased state.
    public static void WeaponPurchased (Weapons.Enum weapon) {
        WeaponPurchasingStatePurchasedEvent?.Invoke(weapon);
    }

    /////////////////////////////////////////////////////////////////
    // P E R K   E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public static void PerkPurchased (Perk.Enum perk) {
        PerkPurchasedEvent?.Invoke(perk);
    }

    /////////////////////////////////////////////////////////////////
    // U P G R A D E   E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    // Handle upgrade purchasing state updated.
    public static void UpgradePurchasingStateUpdated (Weapons.Enum weapon, WeaponUpgrade.Enum upgrade, Purchasing.State state) {
        UpgradePurchasingStateUpdatedEvent?.Invoke(weapon, upgrade, state);
    }

    // Handle weapon locked state.
    public static void UpgradeLocked (Weapons.Enum weapon, WeaponUpgrade.Enum upgrade) {
        UpgradePurchasingStateLockedEvent?.Invoke(weapon, upgrade);
    }

    // Handle weapon purchasable state.
    public static void UpgradePurchasable (Weapons.Enum weapon, WeaponUpgrade.Enum upgrade) {
        UpgradePurchasingStatePurchasableEvent?.Invoke(weapon, upgrade);
    }

    // Handle weapon purchased state.
    public static void UpgradePurchased (Weapons.Enum weapon, WeaponUpgrade.Enum upgrade) {
        UpgradePurchasingStatePurchasedEvent?.Invoke(weapon, upgrade);
    }
}
