using System;
using UnityEngine;

// Lee Barton
public class PerkEventHandler {

    // Perk<Event>
    public static event Action<Perk> PerkActivated;
    public static event Action<Perk> PerkDeactivated;

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    // Handle perk activation.
    public static void Activated (Perk perk) {
        Debug.Log($"EVENT :: Perk Activated :: {perk.Title}");
        PerkActivated?.Invoke(perk);
    }

    // Handle perk deactivation.
    public static void Deactivated (Perk perk) {
        Debug.Log($"EVENT :: Perk Deactivated :: {perk.Title}");
        PerkDeactivated?.Invoke(perk);
    }
}