﻿using System;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class InteractableEventHandler : MonoBehaviour {

    // Interactable<Event>

    public static event Action<bool> InteractableInteracting;

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////
    
    // Handle player interacting with interactable object.
    public static void PlayerInteracting (bool isInteracting) {
        InteractableInteracting?.Invoke(isInteracting);
    }
}
