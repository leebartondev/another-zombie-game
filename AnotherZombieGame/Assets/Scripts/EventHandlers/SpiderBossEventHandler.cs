using System;
using UnityEngine;

// Lee Barton
public class SpiderBossEventHandler : MonoBehaviour {

    public static event Action<float> SpiderBossHealthQuarterDamage;
    public static event Action SpiderBossPassiveAttackComplete;

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    // Spider boss has taken 25% more damage.
    public static void QuarterDamageTaken (float currentHealth) {
        SpiderBossHealthQuarterDamage?.Invoke(currentHealth);
    }

    // Spider boss passive attack has completed.
    public static void PassiveAttackComplete () {
        SpiderBossPassiveAttackComplete?.Invoke();
    }
}
