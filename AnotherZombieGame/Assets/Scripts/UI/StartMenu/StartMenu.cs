using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class StartMenu : MonoBehaviour {

    // @TODO replace "Dev" scene with character selection
    public void OnSinglePlayerButtonClick () {
        SceneHandler.Load(SceneHandler.Scene.CHARACTER_MENU);
    }

    public void OnOptionsButtonClick () {}

    public void OnExitButtonClick () {
        Application.Quit();
    }
}
