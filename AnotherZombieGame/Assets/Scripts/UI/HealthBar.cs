using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Lee Barton
public class HealthBar : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // I N T E R F A C E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private Image bloodImage;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void LateUpdate () {
        this.transform.LookAt(Camera.main.transform);
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void UpdateHealthBarPercentage (float percent) {
        this.bloodImage.fillAmount = percent;
    }

    public void Hide () {
        this.gameObject.SetActive(false);
    }
}