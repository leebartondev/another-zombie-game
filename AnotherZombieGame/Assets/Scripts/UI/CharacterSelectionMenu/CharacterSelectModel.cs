using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class CharacterSelectModel : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public bool Hovering { get; private set; } = false;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private Vector3 targetRotation;
    [SerializeField] private float rotationSpeed = 2.5f;
    [SerializeField] private GameObject modelLights;

    private Vector3 startingRotation;
    private bool selected = false;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        Init();
    }

    private void Update () {
        if (Hovering || selected) {
            if (!Util.Vector3Equal(transform.rotation.eulerAngles, targetRotation)) {
                    transform.rotation = Quaternion.Euler(
                    Vector3.Lerp(transform.rotation.eulerAngles, targetRotation, Time.deltaTime * rotationSpeed)
                 );
                return;
            }
        }

        if (!selected && !Util.Vector3Equal(transform.rotation.eulerAngles, startingRotation)) {
            transform.rotation = Quaternion.Euler(
                Vector3.Lerp(transform.rotation.eulerAngles, startingRotation, Time.deltaTime * rotationSpeed)
            );
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        startingRotation = transform.rotation.eulerAngles;
    }

    public void HandleHover () {
        modelLights.SetActive(true);
        Hovering = true;
    }

    public void HandleHoverExit () {
        Hovering = false;

        if (!selected) {
            modelLights.SetActive(false);
        }
    }

    public void SetSelected (bool selected) {
        this.selected = selected;

        if (!selected && !Hovering) {
            modelLights.SetActive(false);
            Hovering = false;
        }
    }
}