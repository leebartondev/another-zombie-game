using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

// Novasloth Games LLC
// Lee Barton
public class CharacterSelectButton : NovaButton, IPointerDownHandler, IPointerUpHandler {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private PlayerInfoSO playerInfoSO;

    [SerializeField] private Image borderImage;
    [SerializeField] private Color borderDownColor = Color.white;
    private Color borderDefaultColor;

    [SerializeField] private TextMeshProUGUI nameTMP;
    [SerializeField] private Color nameTMPClickedColor = Color.white;
    private Color defaultNameTMPColor;

    [SerializeField] private CharacterSelectionMenu _Menu = null;
    [SerializeField] private CharacterSelectModel _Model;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void SetupReferences () {
        _Menu = GetComponentInParent<CharacterSelectionMenu>();
    }

    protected override void Init () {
        borderDefaultColor = borderImage.color;
        defaultNameTMPColor = nameTMP.color;
    }

    private void ResetButton () {
        if (!clicked) {
            borderImage.enabled = false;
            nameTMP.color = defaultNameTMPColor;
        }
    }

    public void DeselectButton () {
        clicked = false;
        _Model?.SetSelected(false);
        ResetButton();
    }

    public GameObject GetSelectedPlayerGameObject () {
        return playerInfoSO.Prefab;
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public override void OnPointerClick (PointerEventData eventData) {
        clicked = !clicked;
        nameTMP.color = nameTMPClickedColor;
        _Model?.SetSelected(clicked);
        _Menu.OnCharacterSelectButtonClicked(this, clicked, playerInfoSO.Description, playerInfoSO.PerkDescription);
    }

    public override void OnPointerEnter (PointerEventData eventData) {
        borderImage.enabled = true;
        nameTMP.color = nameTMPClickedColor;
        _Model?.HandleHover();

        if (!_Menu.CharacterIsSelected) {
            _Menu.SetCharacterDescriptionText(playerInfoSO.Description, playerInfoSO.PerkDescription);
        }
    }

    public override void OnPointerExit (PointerEventData eventData) {
        ResetButton();
        _Menu.SetCharacterDescriptionText("", "");
        _Model?.HandleHoverExit();
    }

    public void OnPointerDown (PointerEventData eventData) {
        borderImage.color = borderDownColor;
    }

    public void OnPointerUp (PointerEventData eventData) {
        borderImage.color = borderDefaultColor;
    }
}
