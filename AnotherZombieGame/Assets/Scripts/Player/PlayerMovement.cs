﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class PlayerMovement : MonoBehaviour, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // G L O B A L S  /  C O N S T A N T S
    /////////////////////////////////////////////////////////////////

    private const float SPRINT_DIFF = 1.8f;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private float playerSpeed = 5f;

    private Rigidbody playerRigidbody;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.RegisterEventHandlers();
        this.playerRigidbody = this.GetComponent<Rigidbody>();
    }

    private void FixedUpdate () {
        this.Sprint();
        this.Move();
    }

    private void OnDestroy () {
        this.UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Check if the player is sprinting and update speed.
    private void Sprint () {
        if (Movement.IsSprinting(
                (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.JoystickButton8)),
                Input.GetKey(KeyCode.RightShift)
            )) {
            Movement.Instance.PlayerIsSprinting = true;
            Movement.Instance.SetSprintSpeed(this.playerSpeed);
        } else {
            Movement.Instance.PlayerIsSprinting = false;
            Movement.Instance.SetWalkSpeed(this.playerSpeed);
        }
    }

    // Move the player based on the Horizontal and Vertical input axis.
    private void Move () {
        Movement.Instance.HorizontalAxisMovement = Input.GetAxisRaw("Horizontal");
        Movement.Instance.VerticalAxisMovement = Input.GetAxisRaw("Vertical");

        this.playerRigidbody.MovePosition(Movement.CalculateWithInput(
            Input.GetAxisRaw("Horizontal"),
            Input.GetAxisRaw("Vertical"),
            this.transform.position,
            Movement.Instance.PlayerCurrentSpeed
        ));
    }

    // Increase the player's speed by a specified percent
    // Mostly utilized by perk upgrades
    public void IncreaseSpeedByPercent (float percent) {
        this.playerSpeed *= (1.0f + percent);
    }

    // Decrease the player's speed by a specified percent
    // Mostly utilized by perk upgrades
    public void DecreaseSpeedByPercent (float percent) {
        this.playerSpeed /= (1.0f + percent);
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        PlayerEventHandler.PlayerDeathEvent += this.OnPlayerDeathEvent;
    }

    public void UnregisterEventHandlers () {
        PlayerEventHandler.PlayerDeathEvent -= this.OnPlayerDeathEvent;
    }

    public void OnPlayerDeathEvent () {
        this.playerRigidbody.isKinematic = true;
        this.enabled = false;
    }
}
