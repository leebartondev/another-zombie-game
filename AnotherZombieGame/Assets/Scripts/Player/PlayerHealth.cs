using UnityEngine;
using Novasloth;

// Novasloth Games LLC
// Lee Barton
public class PlayerHealth : MonoBehaviour, IAttackable {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public bool IsDead { get { return this.isDead; } }
    public float HealDelay {
        get { return healDelay; }
        set { healDelay = value; }
    }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private float maxHealth = 100.0f;
    [SerializeField] private float healAmount = 5.0f;
    [SerializeField] private float healDelay = 5.0f;

    [SerializeField] private float health;
    private bool isDead = false;

    private Timer healTimer;
    private bool timeToHeal = false;

    private HUD _HUD;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.Init();
        this.SetupReferences();
    }

    private void Start () {
        this.healTimer.Start();
    }

    private void Update () {
        this.HandleTimer();
        this.CheckHeal();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        this.health = maxHealth;

        this.healTimer = new Timer(this.healDelay, () => {
            this.timeToHeal = true;
        });
    }

    private void SetupReferences () {
        this._HUD = this.GetComponentInChildren<HUD>();
    }

    private void HandleTimer () {
        if (this.health < this.maxHealth) {
            this.healTimer.Tick();
        }
    }

    private void CheckHeal () {
        bool needsToHeal = this.health < this.maxHealth;
        if (!this.isDead && this.timeToHeal && needsToHeal) {

            float newHealth = this.health + this.healAmount;
            if (newHealth > this.maxHealth) {
                this.health = this.maxHealth;
            } else {
                this.health = newHealth;
            }

            this._HUD.ResetDamageFlash(this.health / this.maxHealth);
            this.healTimer.Reset();
            this.timeToHeal = false;
            // Debug.Log("HEALED :: " + this.health + "/" + this.maxHealth);
        }
    }

    public void Die () {
        Debug.Log("Player is Dead!");
        this.isDead = true;
        PlayerEventHandler.PlayerDeath();
    }

    public void Attacked (int damage) {
        if (!this.isDead) {
            this.healTimer.Reset();
            this.health -= damage;

            this._HUD.ShowDamageFlash(this.health / this.maxHealth);

            if (this.health <= 0) {
                this.Die();
            }
        }
    }

    public GameObject GetGameObject () {
        return gameObject;
    }

    public void IncreaseHealthByPercent (float percent) {
        this.maxHealth *= (1.0f + percent);
        this.health = this.maxHealth;
    }

    public void DecreaseHealthByPercent (float percent) {
        this.maxHealth /= (1.0f + percent);
        this.health = this.maxHealth;
    }
}
