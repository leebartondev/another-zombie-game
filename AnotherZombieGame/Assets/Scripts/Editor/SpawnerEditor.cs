using UnityEngine;
using UnityEditor;

// Novasloth Games LLC
// Lee Barton
[CustomEditor(typeof(Spawner))]
public class SpawnerEditor : Editor {

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public override void OnInspectorGUI () {
        DrawDefaultInspector();

        Spawner _Spawner = (Spawner)this.target;

        if (GUILayout.Button("Spawn Boss - Random")) {
            _Spawner.SpawnRandomBoss();
        }

        if (GUILayout.Button("Spawn Boss - Zombie")) {
            _Spawner.SpawnZombieBoss();
        }

        if (GUILayout.Button("Spawn Boss - Spider")) {
            _Spawner.SpawnSpiderBoss();
        }

        if (GUILayout.Button("Spawn Interactable - Perk Machine")) {
            _Spawner.SpawnPerkMachine();
        }
    }

}
