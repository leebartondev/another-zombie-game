using System;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
public class DevConsole : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // G L O B A L S  /  C O N S T A N T S
    /////////////////////////////////////////////////////////////////

    private const KeyCode KEY_ENABLE = KeyCode.BackQuote;
    private const KeyCode KEY_SUBMIT = KeyCode.Return;

    /////////////////////////////////////////////////////////////////
    // D E V   C O M M A N D S
    /////////////////////////////////////////////////////////////////

    public static DevCommandWord HELP_COMMAND;
    public static DevCommandWord DIE_COMMAND;

    public static DevCommandWord<bool> ENABLE_COMMAND;

    public static DevCommandWord<Perk.Enum> PURCHASE_PERK_COMMAND;

    public static DevCommandWord<Weapons.Enum, WeaponUpgrade.Enum> PURCHASE_UPGRADE_COMMAND;

    public static DevCommandWord<int> ADD_BRAINS_COMMAND;
    public static DevCommandWord<int> USE_BRAINS_COMMAND;

    public static DevCommandWord<Enemy.Zombie> SPAWN_ENEMY_COMMAND;
    public static DevCommandWord<Enemy.Boss> SPAWN_BOSS_COMMAND;

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public bool IsActive { get; set; } = false;

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private PerkMachine _PerkMachine;

    private bool isHelpActive = false;
    private bool canHandleCommands = false;

    private string consoleInput = "";
    private float consoleYPos = 0.0f;
    private Vector2 scrollPosition;

    private List<DevCommand> devCommandList;

    private WeaponsUpgradePurchasing _UpgradePurchasing;
    private WeaponCursor _WeaponCursor;
    private PlayerStats _PlayerStats;
    private PlayerHealth _PlayerHealth;
    private Spawner _Spawner;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        SetupReferences();
        Init();
    }

    private void Update () {
        ListenForKeyPress();
    }

    private void ListenForKeyPress () {
        if (Input.GetKeyDown(KEY_ENABLE)) {
            ToggleConsole();
        }

        if (Input.GetKeyDown(KEY_SUBMIT) && IsActive) {
            SubmitCommand();
        }
    }

    private void OnGUI () {
        Event e = Event.current;

        if (IsActive) {
            // Check input while focused
            if (e.type == EventType.KeyDown) {
                if (e.keyCode == KEY_SUBMIT) {
                    SubmitCommand();
                } else if (e.keyCode == KEY_ENABLE) {
                    ToggleConsole();
                }
            }

            consoleYPos = 0.0f;

            if (isHelpActive) {
                GUI.Box(new Rect(0, consoleYPos, Screen.width, 100), "");
                Rect viewport = new Rect(0, 0, Screen.width - 30, 20 * devCommandList.Count);

                scrollPosition = GUI.BeginScrollView(
                    new Rect(0, consoleYPos + 5.0f, Screen.width, 90),
                    scrollPosition,
                    viewport
                );

                for (int i = 0; i < devCommandList.Count; i++) {
                    string label = $"{devCommandList[i].CommandFormat} :: {devCommandList[i].CommandDescription}";

                    Rect labelRect = new Rect(5, 20 * i, viewport.width - 100, 20);

                    GUI.Label(labelRect, label);
                }
                GUI.EndScrollView();

                consoleYPos += 100.0f;
            }

            // Console window
            GUI.Box(new Rect(0.0f, consoleYPos, Screen.width, 30.0f), "");
            GUI.backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);

            // Console input
            GUI.SetNextControlName("DevCommandInputField");
            consoleInput = GUI.TextField(
                new Rect(10.0f, consoleYPos + 5.0f, Screen.width - 20.0f, 20.0f),
                consoleInput
            );
            GUI.FocusControl("DevCommandInputField");
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Init () {
        CreateCommands();
    }

    private void SetupReferences () {
        GameObject player = Util.GetPlayerByTag();

        if (Util.IsNull(player)) {
            Debug.LogError("DevConsole :: SetupReferences() :: Player is NULL! Disabling...");
            enabled = false;
            return;
        }

        GameObject spawner = Util.GetSpawnerByTag();

        _UpgradePurchasing = player.GetComponentInChildren<WeaponsUpgradePurchasing>();
        _WeaponCursor = player.GetComponentInChildren<WeaponCursor>();
        _PlayerStats = player.GetComponent<PlayerStats>();
        _PlayerHealth = player.GetComponent<PlayerHealth>();
        _Spawner = spawner.GetComponent<Spawner>();
    }

    private void ToggleConsole () {
        IsActive = !IsActive;

        if (!_PlayerHealth.IsDead) {
            _WeaponCursor.ToggleCrosshair(!IsActive);
        }
    }

    private void SubmitCommand () {
        HandleCommand();
        consoleInput = "";
    }

    private void CreateCommands () {
        // debug <bool>
        ENABLE_COMMAND = new DevCommandWord<bool>(
            "debug",
            "Enable developer console commands.",
            "debug <bool>",
            (value) => {
                canHandleCommands = value;
                Debug.Log("CONSOLE_COMMAND :: debug :: " + (value ? "ENABLED" : "DISABLED"));
            }
        );

        // help
        HELP_COMMAND = new DevCommandWord(
            "help",
            "List all possible commands.",
            "help",
            () => {
                isHelpActive = true;
            }
        );

        // die
        DIE_COMMAND = new DevCommandWord(
            "die",
            "Kill the player.",
            "die",
            () => {
                _PlayerHealth.Die();
            }
        );

        // purchase perk <type>
        PURCHASE_PERK_COMMAND = new DevCommandWord<Perk.Enum>(
            "purchase perk",
            "Purchase a specific perk.",
            "purchase perk <type>",
            (perkType) => {
                if (!Util.IsNull(_PerkMachine)) {
                    _PerkMachine.PurchasePerk(perkType);
                }
            }
        );

        // purchase upgrade <weapon type> <upgrade type>
        PURCHASE_UPGRADE_COMMAND = new DevCommandWord<Weapons.Enum, WeaponUpgrade.Enum>(
            "purchase upgrade",
            "Purchase a specific weapon upgrade.",
            "purchase upgrade <weapon> <upgrade>",
            (weaponType, upgradeType) => {
                Debug.Log(weaponType + ", " + upgradeType);
                if (!Util.IsNull(_UpgradePurchasing)) {
                    Debug.Log($"DevConsole :: Purchasing upgrade: {weaponType}, {upgradeType}");
                    _UpgradePurchasing.PurchaseUpgrade(weaponType, upgradeType);
                }
            }
        );

        // add brains <amount>
        ADD_BRAINS_COMMAND = new DevCommandWord<int>(
            "add brains",
            "Add a specified amount of brains to the player stats.",
            "add brains <amount>",
            (amount) => {
                _PlayerStats.AddBrains(amount);
            }
        );

        // use brains <amount>
        USE_BRAINS_COMMAND = new DevCommandWord<int>(
            "use brains",
            "Use a specified amount of brains to the player stats.",
            "use brains <amount>",
            (amount) => {
                _PlayerStats.UseBrains(amount);
            }
        );

        // spawn boss <type>
        SPAWN_BOSS_COMMAND = new DevCommandWord<Enemy.Boss>(
            "spawn boss",
            "Spawn a specific boss at random spawn point.",
            "spawn boss <type>",
            (bossType) => {
                _Spawner.SpawnBoss(bossType);
            }
        );

        SPAWN_ENEMY_COMMAND = new DevCommandWord<Enemy.Zombie>(
            "spawn enemy",
            "Spawn a specific enemy at random spawn point.",
            "spawn enemy <type>",
            (enemyType) => {
                _Spawner.SpawnEnemy(enemyType);
            }
        );

        // spawn enemy <type>

        devCommandList = new List<DevCommand> {
            ENABLE_COMMAND,
            HELP_COMMAND,
            DIE_COMMAND,
            PURCHASE_PERK_COMMAND,
            PURCHASE_UPGRADE_COMMAND,
            ADD_BRAINS_COMMAND,
            USE_BRAINS_COMMAND,
            SPAWN_BOSS_COMMAND,
            SPAWN_ENEMY_COMMAND
        };
    }

    private void HandleCommand () {
        string[] properties = consoleInput.Split(' ');

        if (consoleInput.Contains(ENABLE_COMMAND.CommandId) && properties.Length > 1) {
            try {
                ENABLE_COMMAND.Invoke(bool.Parse(properties[1]));
            } catch (Exception e) {
                Debug.LogWarning($"CONSOLE_COMMAND :: ERROR :: {e.Message}");
            }
        } else {
            for (int i = 0; i < devCommandList.Count; i++) {
                DevCommand devCommand = devCommandList[i];

                canHandleCommands = true; // @TODO remove
                if (canHandleCommands && consoleInput.Contains(devCommand.CommandId)) {

                    Util.TypeSwitch.On(devCommand)
                    .Case((DevCommandWord<Weapons.Enum, WeaponUpgrade.Enum> command) => {
                        try {
                            command.Invoke(
                                (Weapons.Enum)Enum.Parse(typeof(Weapons.Enum), properties[2].ToUpper()),
                                (WeaponUpgrade.Enum)Enum.Parse(typeof(WeaponUpgrade.Enum), properties[3].ToUpper())
                            );
                        } catch (Exception e) {
                            Debug.LogWarning($"CONSOLE_COMMAND :: ERROR :: {e.Message}");
                        }
                    })
                    .Case((DevCommandWord<Perk.Enum> command) => {
                        try {
                            command.Invoke(
                                (Perk.Enum)Enum.Parse(typeof(Perk.Enum), properties[2].ToUpper())
                            );
                        } catch (Exception e) {
                            Debug.LogWarning($"CONSOLE_COMMAND :: ERROR :: {e.Message}");
                        }
                    })
                    .Case((DevCommandWord<Enemy.Boss> command) => {
                        try {
                            if (properties.Length < 3) { // spawn boss
                                command.Invoke(
                                    Enemy.Boss.RANDOM
                                );
                            } else { // spawn boss <type>
                                command.Invoke(
                                    (Enemy.Boss)Enum.Parse(typeof(Enemy.Boss), properties[2].ToUpper())
                                );
                            }
                        } catch (Exception e) {
                            Debug.LogWarning($"CONSOLE_COMMAND :: ERROR :: {e.Message}");
                        }
                    })
                    .Case((DevCommandWord<Enemy.Zombie> command) => {
                        try {
                            if (properties.Length < 3) { // spawn enemy
                                command.Invoke(
                                    Enemy.Zombie.RANDOM
                                );
                            } else { // spawn enemy <type>
                                command.Invoke(
                                    (Enemy.Zombie)Enum.Parse(typeof(Enemy.Zombie), properties[2].ToUpper())
                                );
                            }
                        }
                        catch (Exception e) {
                            Debug.LogWarning($"CONSOLE_COMMAND :: ERROR :: {e.Message}");
                        }
                    })
                    .Case((DevCommandWord<int> command) => {
                        try {
                            command.Invoke(int.Parse(properties[2]));
                        } catch (Exception e) {
                            Debug.LogWarning($"CONSOLE_COMMAND :: ERROR :: {e.Message}");
                        }
                    })
                    .Case((DevCommandWord<bool> command) => {
                        try {
                            command.Invoke(bool.Parse(properties[1]));
                        } catch (Exception e) {
                            Debug.LogWarning($"CONSOLE_COMMAND :: ERROR :: {e.Message}");
                        }
                    })
                    .Case((DevCommandWord command) => {
                        command.Invoke();
                    });

                }
            }
        }
    }
}
