using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

// Lee Barton
// CREDIT: DitzelGames https://github.com/ditzel/SimpleIK
public class IK : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public Vector3 TargetPosition { get { return this.target.position; } }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Bone Information")]
    [Tooltip("Total length of bones.")]
    [SerializeField] private int chainLength = 2;

    [Tooltip("Target the chain should bend to.")]
    [SerializeField] private Transform target;
    [Tooltip("Parent to set target to.")]
    [SerializeField] private Transform targetParent;

    [Tooltip("Elbow direction chain bends at.")]
    [SerializeField] private Transform pole;

    [Header("Solver Iteration")]
    [Tooltip("Total solver iterations per update.")]
    [SerializeField] private int totalIterations = 10;

    [Tooltip("Margin of error in distance allowed for solver to stop.")]
    [SerializeField] private float delta = 0.001f;

    [Tooltip("Strength of snapping back to start position.")]
    [Range(0, 1)]
    [SerializeField] private float snapBackStrength = 1.0f;

    [Header("Debug")]
    [SerializeField] private bool debug = false;

    private float[] bonesLength;    // Target to origin
    private float completeLength;   // Length of all bone vectors
    private Transform[] bones;
    private Vector3[] positions;

    // Starting states
    private Vector3[] startDirectionToChild;
    private Quaternion[] startBoneRotations;
    private Quaternion startTargetRotation;
    private Quaternion startRootRotation;

    // Moving IK target to foot target
    private float startTargetPositionY;
    private Vector3 positionAtStartHeight;
    private Vector3 footTargetPositionOffset;
    private Vector3 footTargetPositionCurve;
    // https://forum.unity.com/threads/vector3-and-other-structs-optimization-of-operators.477338/
    // ^ Why I don't use new Vector3()

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.Init();
    }

    private void LateUpdate () {
        this.DoIK();
    }

    private void OnDrawGizmos () {
        if (this.debug) {
            // Iterate through the bone transforms
            this.DrawWireBoneHandles();
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Initialize data
    private void Init () {
        this.bones = new Transform[this.chainLength + 1];   // Transforms (3 chains means 4 bones -> BONE --- BONE --- BONE --- BONE)
        this.positions = new Vector3[this.chainLength + 1];
        this.bonesLength = new float[this.chainLength];

        this.startDirectionToChild = new Vector3[this.chainLength + 1];
        this.startBoneRotations = new Quaternion[this.chainLength + 1];

        if (this.target == null) {
            this.target = new GameObject(this.gameObject.name + "_Target").transform;
            this.target.position = this.transform.position;
        }

        if (this.targetParent != null) {
            this.target.SetParent(this.targetParent);
        }

        this.startTargetRotation = this.target.rotation;
        this.startTargetPositionY = this.target.position.y;
        this.completeLength = 0;

        Transform currentTransform = this.transform;
        int lastIndex = this.bones.Length - 1;
        for (int i = lastIndex; i >= 0; i--) {
            this.bones[i] = currentTransform;
            this.startBoneRotations[i] = currentTransform.rotation;

            if (i == lastIndex) { // "leaf" bone (last bone) - has no length
                this.startDirectionToChild[i] = this.target.position - currentTransform.position;
            } else { // Mid bone
                Vector3 boneDistance = this.bones[i + 1].position - currentTransform.position;
                this.startDirectionToChild[i] = boneDistance;
                this.bonesLength[i] = boneDistance.magnitude;
                this.completeLength += boneDistance.magnitude;
            }

            currentTransform = currentTransform.parent;
        }
    }

    // Do inverse kinematics
    private void DoIK () {
        // Don't do anything if target is not set
        if (this.target == null) {
            return;
        }

        // Chain length does not match bone length then reinitialize
        if (this.bonesLength.Length != this.chainLength) {
            Debug.Log("RE-INIT");
            this.Init();
        }

        // FABRIK algorithm - Get position
        // (Forward And Backward Reaching IK)
        for (int i = 0; i < this.bones.Length; i++) {
            this.positions[i] = this.bones[i].position; // DO NOT do calcs on bone positions directly
        }

        Quaternion rootRotation = (this.bones[0].parent != null) ? this.bones[0].parent.rotation : Quaternion.identity;
        Quaternion rootRotationDifference = rootRotation * Quaternion.Inverse(this.startRootRotation);

        // Do calculations on positions
        // Is it possible to reach target? Get squared distance from target to first bone (squared calc is faster)
        float distanceFromTarget = (this.target.position - this.bones[0].position).sqrMagnitude;
        if (distanceFromTarget >= (this.completeLength * this.completeLength)) {
            // "stretch it"
            // Distribute bones along calculated direction from first bone to target
            Vector3 direction = (this.target.position - this.positions[0]).normalized;

            // Set everything after the root bone
            for (int i = 1; i < this.positions.Length; i++) {
                this.positions[i] = this.positions[i - 1] + (direction * this.bonesLength[i - 1]);
            }
        }
        else {
            for (int i = 0; i < this.positions.Length - 1; i++) {
                this.positions[i + 1] = Vector3.Lerp(
                    this.positions[i + 1],
                    this.positions[i] + (rootRotationDifference * this.startDirectionToChild[i]),
                    this.snapBackStrength
                );
            }

            // Being solver iterations
            for (int i = 0; i < this.totalIterations; i++) {
                // FABRIK - Backward Reaching
                for (int j = this.positions.Length - 1; j > 0; j--) {
                    if (j == this.positions.Length - 1) { // Beginning of bone sequence
                        this.positions[j] = this.target.position; // Set last bone to target
                    } else {
                        // Set bones correct distance apart specified by bone's length
                        this.positions[j] = this.positions[j + 1] + ((this.positions[j] - this.positions[j + 1]).normalized * this.bonesLength[j]);
                    }
                }

                // FABRIK - Forward Reaching
                for (int j = 1; j < this.positions.Length; j++) {
                    this.positions[j] = this.positions[j - 1] + ((this.positions[j] - this.positions[j - 1]).normalized * this.bonesLength[j - 1]);
                }

                // Check if we are within margin of error
                float distanceFromLastToTarget = (this.positions[this.positions.Length - 1] - this.target.position).sqrMagnitude;
                if (distanceFromLastToTarget < (this.delta * this.delta)) {
                    break;
                }
            }
        }

        // Rotate elbow towards pole using closest point values on plane
        if (this.pole != null) {
            for (int i = 1; i < this.positions.Length - 1; i++) {
                Plane plane = new Plane(this.positions[i + 1] - this.positions[i - 1], this.positions[i - 1]);
                Vector3 projectedPoleVector = plane.ClosestPointOnPlane(this.pole.position);
                Vector3 projectedBoneVector = plane.ClosestPointOnPlane(this.positions[i]);
                float angleBetweenPoleAndBone = Vector3.SignedAngle(projectedBoneVector - this.positions[i - 1], projectedPoleVector - this.positions[i - 1], plane.normal);
                this.positions[i] = (Quaternion.AngleAxis(angleBetweenPoleAndBone, plane.normal) * (this.positions[i] - this.positions[i - 1])) + this.positions[i - 1];
            }
        }

        // Set bone positions and rotations
        for (int i = 0; i < this.positions.Length; i++) {
            if (i == this.positions.Length - 1) {
                this.bones[i].rotation = this.target.rotation * Quaternion.Inverse(this.startTargetRotation) * this.startBoneRotations[i];
            } else {
                this.bones[i].rotation = Quaternion.FromToRotation(this.startDirectionToChild[i], (this.positions[i + 1] - this.positions[i])) * this.startBoneRotations[i];
            }

            this.bones[i].position = this.positions[i];
        }
    }

    // Draw handles representing the bones tarting from the last child bone transform
    private void DrawWireBoneHandles () {
        Transform currentTransform = this.transform;
        for (int i = 0; i < this.chainLength && currentTransform != null && currentTransform.parent != null; i++) {
            float scale = Vector3.Distance(currentTransform.position, currentTransform.parent.position) * 0.1f;

            Handles.matrix = Matrix4x4.TRS(
                currentTransform.position,
                Quaternion.FromToRotation(
                    Vector3.up,
                    currentTransform.parent.position - currentTransform.position
                ),
                new Vector3(
                    scale,
                    Vector3.Distance(currentTransform.parent.position, currentTransform.position),
                    scale
                )
            );
            Handles.color = Color.cyan;
            Handles.DrawWireCube(Vector3.up * 0.5f, Vector3.one);

            currentTransform = currentTransform.parent;
        }
    }

    /////////////////////////////////////////////////////////////////
    // P U B L I C   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Move the IK target to the foot target position
    public bool MoveIKTargetToFootTarget (Vector3 moveToPosition, AnimationCurve curve, float moveSpeed, float minDistance, float maxDistance, float error, bool debug) {
        float step = moveSpeed * Time.deltaTime;

        this.positionAtStartHeight.Set(
            this.target.position.x,
            this.startTargetPositionY,
            this.target.position.z
        );

        this.footTargetPositionOffset.Set(
            moveToPosition.x,
            this.startTargetPositionY,
            moveToPosition.z //+ directionOffset
        );

        if (debug) { Debug.DrawLine(this.positionAtStartHeight, this.footTargetPositionOffset); }

        // Normalize the distance from IK target position at starting height to foot target position offset height
        float normalizedDistance = (
            maxDistance - Vector3.Distance(
                this.positionAtStartHeight,
                this.footTargetPositionOffset
            )
        ) / (maxDistance - minDistance);
        float yValueOfCurve = curve.Evaluate(normalizedDistance);
        if (debug) { Debug.Log("Y ON CURVE: " + yValueOfCurve + " @ " + normalizedDistance); }

        this.footTargetPositionCurve.Set(
            moveToPosition.x,
            footTargetPositionOffset.y + moveToPosition.y + yValueOfCurve,
            moveToPosition.z
        );
        // Debug.Log("MOVING LEG!");
        this.target.position = Vector3.MoveTowards(
            this.target.position,
            this.footTargetPositionCurve,
            step
        );

        this.footTargetPositionOffset.Set(
            moveToPosition.x,
            this.footTargetPositionOffset.y + moveToPosition.y,
            moveToPosition.z
        );

        if (debug) {
            Debug.Log(Vector3.Distance(this.target.position, this.footTargetPositionOffset));
            Debug.Log(this.target.position + " : " + this.footTargetPositionOffset);
            Debug.Log("Foot @ Target: " + (Vector3.Distance(this.target.position, this.footTargetPositionOffset) < error));
        }
        return Vector3.Distance(this.target.position, this.footTargetPositionOffset) < error;
    }

    // Move the initial IK target to starting offset position
    public void OffsetIKTarget (float offset) {
        this.target.position += (this.target.forward * offset);
    }
}
