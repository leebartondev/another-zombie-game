using UnityEngine;

// Lee Barton
public class ZombieBossAnimation : BossAnimation, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private GameObject spawnDirtPE;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void Awake () {
        base.Awake();
        RegisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void CheckWalking () {
        bool hasTarget = _EnemyMovement.HasTarget;
        animator.SetBool(MyAnimation.HAS_TARGET, hasTarget);
    }

    public void StompAttack () {
        this.animator.SetTrigger(MyAnimation.STOMP_ATTACK);
    }

    public void DisableAnimator () {
        if (this.animator.isActiveAndEnabled) {
            this.animator.enabled = false;
        }
    }

    public void TriggerSpawnStart () {
        if (Util.IsNull(this.animator) && !this.referencesSetup) {
            this.SetupReferences();
            this.referencesSetup = true;
        }

        if (!this.spawnDirtPE.activeInHierarchy) {
            this.spawnDirtPE.SetActive(true);
        }

        this.animator.SetTrigger(MyAnimation.SPAWN_START);
    }

    private void TriggerSpawnComplete () {
        this.SpawnAnimationComplete = true;
        this.spawnDirtPE.SetActive(false);
        this.animator.SetTrigger(MyAnimation.SPAWN_COMPLETE);
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        AnimationEventHandler.AnimationZombieBossSpawnEnd
            += this.OnZombieBossSpawnAnimationEnd;
    }

    public void UnregisterEventHandlers () {
        AnimationEventHandler.AnimationZombieBossSpawnEnd
            -= this.OnZombieBossSpawnAnimationEnd;
    }

    private void OnZombieBossSpawnAnimationEnd () {
        this.TriggerSpawnComplete();
    }
}
