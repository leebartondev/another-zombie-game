using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
public class MiniSpiderExplode : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private GameObject gasPrefab;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void OnTriggerEnter (Collider other) {
        if (Util.IsPlayer(other)) {
            this.Explode();
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void Explode () {
        GameObject gas = Instantiate(this.gasPrefab);
        gas.transform.position = this.transform.position;
        Destroy(this.gameObject);
    }

}
