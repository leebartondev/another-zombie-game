using UnityEngine;
using Novasloth;

// Lee Barton
public class SpiderPoisonGas : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    // @TODO Damage increases with rounds (non-linear / exponential)

    [SerializeField] private int damage = 10;
    [SerializeField] private float damageDelay = 2.5f;
    [SerializeField] private float timeToDamage = 15.0f;

    private PlayerHealth _PlayerHealth;

    private Timer lifeTimer;
    private Timer damageTimer;
    private bool canDamagePlayer = false;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.lifeTimer = new Timer(this.timeToDamage, () => {
            this.enabled = false;
        });

        this.damageTimer = new Timer(0.0f, () => {
            this.canDamagePlayer = true;
            this.damageTimer.TimerDelay = this.damageDelay;
        });
    }

    private void Start () {
        this.lifeTimer.Start();
        this.damageTimer.Start();
    }

    private void Update () {
        //this.HandleTimer();
        this.lifeTimer.Tick();
        this.damageTimer.Tick();
    }

    private void OnTriggerEnter (Collider other) {
        if (Util.IsPlayer(other)) {
            this.DamagePlayer(other.gameObject);
        }
        // @TODO check for enemies
    }

    private void OnTriggerStay (Collider other) {
        if (Util.IsPlayer(other)) {
            this.DamagePlayer(other.gameObject);
        }
    }

    private void OnTriggerExit (Collider other) {
        if (Util.IsPlayer(other) && !Util.IsNull(this._PlayerHealth)) {
            this._PlayerHealth = null;
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void DamagePlayer (GameObject player) {
        if (this.canDamagePlayer) {
            if (Util.IsNull(_PlayerHealth)) {
                this._PlayerHealth = player.GetComponent<PlayerHealth>();
            }

            _PlayerHealth.Attacked(this.damage);
            this.canDamagePlayer = false;
            this.damageTimer.Reset();
        }
    }
}
