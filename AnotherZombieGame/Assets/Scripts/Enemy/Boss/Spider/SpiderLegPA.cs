using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
public class SpiderLegPA : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // G L O B A L   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    private const float TARGET_RAYCAST_MAX_DISTANCE = 25.0f;
    private const float TARGET_HIT_ERROR = 0.001f;

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public bool IsGrounded { get { return !this.moveLeg; } }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("'Foot' Transforms")]
    [SerializeField] private Transform footTransform;
    [SerializeField] private Transform footTargetTransform;
    [SerializeField] private Transform footTargetRaycastTransform;

    [Header("Leg Properties")]
    [SerializeField] private AnimationCurve stepCurve;
    [SerializeField] private float distanceToMoveFoot = 0.55f;
    [SerializeField] private float stepSpeed = 1.0f;

    [Header("Offset Values")]
    [SerializeField] private float IKTargetStartOffset = 0.0f;
    [SerializeField] private float footTargetOffset = 0.0f;
    [SerializeField] private float footTargetAtDistanceOffset = 0.0f;

    [Header("Dependent Legs")]
    [SerializeField] private SpiderLegPA[] dependentLegs;

    [Header("Debug")]
    [SerializeField] private bool debug = false;

    private float targetDistanceFromFoot;
    private RaycastHit targetRaycastHit;
    private Vector3 targetNewPosition;

    // Foot target offset positions
    private Vector3 targetOffsetNewPosition;
    private Vector3 footTargetOffsetRaycastPosition;
    private Vector3 footTargetPositionLastFrame;
    private Transform footTargetOffsetForwardTransform;
    private Transform footTargetOffsetBackwardTransform;
    private float forwardTest = 0;

    private Vector3? targetPositionAtMaxDistance = null;

    private bool moveLeg = false;

    private IK _IK;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        this.SetupReferences();
        this.Init();
    }

    private void Start () {
        this.SetFootTargetStartPosition();

        if (this.footTargetAtDistanceOffset != 0) {
            this.SetFootTargetForwardOffsetStartPosition();
            this.SetFootTargetBackwardOffsetStartPosition();
        }

        this.SetIKTargetStartOffset();
    }

    private void Update () {
        this.MoveLeg();
        this.GetTargetDistanceFromFoot();
    }

    private void LateUpdate () {
        this.AdjustTargetPosition();

        this.footTargetPositionLastFrame = this.footTargetTransform.position;
    }

    private void OnDrawGizmos () {
        if (this.debug) {
            Gizmos.color = Color.red;
            if (this.footTargetTransform != null) {
                Gizmos.DrawSphere(this.footTargetTransform.position, 0.1f);
            }

            bool offsetForwardTransformExists = this.footTargetOffsetForwardTransform != null;
            bool offsetBackwardTransformExists = this.footTargetOffsetBackwardTransform != null;
            bool offsetTransformsExist = offsetForwardTransformExists && offsetBackwardTransformExists;
            if (this.footTargetAtDistanceOffset != 0 && offsetTransformsExist) {
                Gizmos.color = Color.green;
                Gizmos.DrawSphere (this.footTargetOffsetForwardTransform.position, 0.05f);
                Gizmos.DrawSphere(this.footTargetOffsetBackwardTransform.position, 0.05f);
            }

            if (this.targetPositionAtMaxDistance != null) {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere((Vector3)this.targetPositionAtMaxDistance, 0.1f);
            }
        }
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void SetupReferences () {
        this._IK = this.GetComponentInChildren<IK>();
    }

    private void Init () {
        if (this.footTargetTransform == null) {
            this.CreateFootTarget();
        }
    }

    private void MoveLeg () {
        if (this.debug) { Debug.Log("MOVE_LEG: " + this.forwardTest + " :: " + this.moveLeg); }
        if (this.moveLeg) {
            if (this.targetPositionAtMaxDistance == null) {
                if (this.forwardTest < 0.0f) {
                    this.targetPositionAtMaxDistance = this.footTargetOffsetBackwardTransform.position;
                } else if (this.forwardTest > 0.0f) {
                    this.targetPositionAtMaxDistance = this.footTargetOffsetForwardTransform.position;
                } else {
                    this.targetPositionAtMaxDistance = this.footTargetTransform.position;
                }
            }

            if (this._IK.MoveIKTargetToFootTarget(
                (Vector3)this.targetPositionAtMaxDistance,
                this.stepCurve,
                this.stepSpeed,
                0,
                this.distanceToMoveFoot,
                TARGET_HIT_ERROR,
                this.debug
            )) {
                this.moveLeg = false;
                this.targetPositionAtMaxDistance = null;
            }
        }
    }

    private void AdjustTargetPosition () {
        float directionCheck = ((this.footTargetTransform.position - this.footTargetPositionLastFrame).normalized).z;
        if (directionCheck != 0) {
            this.forwardTest = directionCheck;
        }
        Debug.DrawLine(this.footTargetTransform.position, this.footTargetPositionLastFrame, Color.black);
        // Debug.Log("ADJUST_TARGET_POSITION: " + this.forwardTest);

        if (this.forwardTest < 0.0f) { if (this.debug) Debug.Log("Raycast Backward!");
            this.RaycastBackwardOffsetTransform();
        } else if (this.forwardTest > 0.0f) { if (this.debug) Debug.Log("Raycast Forward!");
            this.RaycastForwardOffsetTransform();
        } else {
            this.RaycastTargetTransform();
        }
    }

    // Raycast for forward offset
    private void RaycastForwardOffsetTransform () {
        if (this.DoOffsetRaycast(this.footTargetOffsetForwardTransform)) {
            this.SetFootTargetForwardOffsetPosition(
                this.footTargetOffsetForwardTransform.position.x,
                this.targetRaycastHit.point.y,
                this.footTargetOffsetForwardTransform.position.z
            );
        }
    }

    // Raycast for backward offset
    private void RaycastBackwardOffsetTransform () {
        if (this.DoOffsetRaycast(this.footTargetOffsetBackwardTransform)) {
            this.SetFootTargetBackwardOffsetPosition(
                this.footTargetOffsetBackwardTransform.position.x,
                this.targetRaycastHit.point.y,
                this.footTargetOffsetBackwardTransform.position.z
            );
        }
    }

    // Handle raycast for offset Transforms
    private bool DoOffsetRaycast (Transform offsetTransform) {
        this.footTargetOffsetRaycastPosition.Set(
            offsetTransform.position.x,
            this.footTargetRaycastTransform.position.y,
            offsetTransform.position.z
        );

        if (Physics.Raycast(
            this.footTargetOffsetRaycastPosition,
            Vector3.down,
            out this.targetRaycastHit,
            TARGET_RAYCAST_MAX_DISTANCE
        )) {
            if (this.debug) { Debug.Log($"INFO :: SpiderLegPA :: {this.gameObject.name} :: {this.targetRaycastHit.collider.name}"); }
            Debug.DrawRay(this.footTargetOffsetRaycastPosition, Vector3.down * TARGET_RAYCAST_MAX_DISTANCE, Color.green);
            if (Mathf.Abs(offsetTransform.position.y - this.targetRaycastHit.point.y) > TARGET_HIT_ERROR && !this.targetRaycastHit.collider.isTrigger) {
                this.SetFootTargetPosition(
                    this.footTargetTransform.position.x,
                    this.targetRaycastHit.point.y,
                    this.footTargetTransform.position.z
                );
                return true;
            }
        } else {
            Debug.DrawRay(this.footTargetOffsetRaycastPosition, Vector3.down * TARGET_RAYCAST_MAX_DISTANCE, Color.red);
        }

        return false;
    }

    // Raycast with no offset
    private void RaycastTargetTransform () {
        if (Physics.Raycast(
            this.footTargetRaycastTransform.position,
            this.footTargetRaycastTransform.TransformDirection(Vector3.down),
            out this.targetRaycastHit,
            TARGET_RAYCAST_MAX_DISTANCE
        )) {
            Debug.DrawRay(this.footTargetRaycastTransform.position, this.footTargetRaycastTransform.TransformDirection(Vector3.down) * TARGET_RAYCAST_MAX_DISTANCE, Color.green);
            if (Mathf.Abs(this.footTargetTransform.position.y - this.targetRaycastHit.point.y) > TARGET_HIT_ERROR && !this.targetRaycastHit.collider.isTrigger) {
                this.SetFootTargetPosition(
                    this.footTargetTransform.position.x,
                    this.targetRaycastHit.point.y,
                    this.footTargetTransform.position.z
                );
            }
        } else {
            Debug.DrawRay(this.footTargetRaycastTransform.position, this.footTargetRaycastTransform.TransformDirection(Vector3.down) * TARGET_RAYCAST_MAX_DISTANCE, Color.red);
        }
    }

    private void GetTargetDistanceFromFoot () {
        bool dependentLegsGrounded = true;
        if (this.dependentLegs.Length > 0) {
            foreach (SpiderLegPA leg in this.dependentLegs) {
                if (!leg.IsGrounded) {
                    dependentLegsGrounded = false;
                    break;
                }
            }
        }

        this.targetDistanceFromFoot = Vector3.Distance(this.footTargetTransform.position, this.footTransform.position);
        if (this.debug) { Debug.Log("TARGET_DISTANCE: " + this.forwardTest + " :: " + this.targetDistanceFromFoot); }
        if (this.targetDistanceFromFoot >= this.distanceToMoveFoot && dependentLegsGrounded) {
            this.moveLeg = true;
        }
    }

    private void CreateFootTarget () {
        Transform legsParent = this.transform.parent;

        this.footTargetTransform = new GameObject(this.gameObject.name + "_FootTarget").transform;
        this.footTargetTransform.parent = legsParent;

        this.footTargetRaycastTransform = new GameObject(this.footTargetTransform.name + "_Raycast").transform;
        this.footTargetRaycastTransform.parent = this.footTargetTransform;
        this.footTargetRaycastTransform.localPosition = new Vector3(0, 1, 0);

        if (this.footTargetAtDistanceOffset != 0) {
            this.footTargetOffsetForwardTransform = new GameObject(this.footTargetTransform.name + "_OffsetForward").transform;
            this.footTargetOffsetForwardTransform.parent = this.footTargetTransform;

            this.footTargetOffsetBackwardTransform = new GameObject(this.footTargetTransform.name + "_OffsetBackward").transform;
            this.footTargetOffsetBackwardTransform.parent = this.footTargetTransform;
        }
    }

    private void SetFootTargetStartPosition () {
        this.SetFootTargetPosition(
            this.footTransform.position.x,
            this.footTargetTransform.position.y,
            this.footTransform.position.z
        );
        this.footTargetTransform.localPosition += (this.transform.parent.forward * this.footTargetOffset);
    }

    private void SetFootTargetForwardOffsetStartPosition () {
        this.SetFootTargetForwardOffsetPosition(
            this.footTargetTransform.position.x,
            this.footTargetTransform.position.y,
            this.footTargetTransform.position.z
        );
        this.footTargetOffsetForwardTransform.localPosition += (this.footTargetTransform.forward * this.footTargetAtDistanceOffset);
    }

    private void SetFootTargetBackwardOffsetStartPosition () {
        this.SetFootTargetBackwardOffsetPosition(
            this.footTargetTransform.position.x,
            this.footTargetTransform.position.y,
            this.footTargetTransform.position.z
        );
        this.footTargetOffsetBackwardTransform.localPosition += (this.footTargetTransform.forward * (-1 * this.footTargetAtDistanceOffset));
    }

    private void SetFootTargetPosition (float x, float y, float z) {
        this.targetNewPosition.Set(x, y, z);
        this.footTargetTransform.position = this.targetNewPosition;
    }

    private void SetFootTargetForwardOffsetPosition (float x, float y, float z) {
        this.targetOffsetNewPosition.Set(x, y, z);
        this.footTargetOffsetForwardTransform.position = this.targetOffsetNewPosition;
    }

    private void SetFootTargetBackwardOffsetPosition (float x, float y, float z) {
        this.targetOffsetNewPosition.Set(x, y, z);
        this.footTargetOffsetBackwardTransform.position = this.targetOffsetNewPosition;
    }

    private void SetIKTargetStartOffset () {
        this._IK.OffsetIKTarget(this.IKTargetStartOffset);
    }
}
