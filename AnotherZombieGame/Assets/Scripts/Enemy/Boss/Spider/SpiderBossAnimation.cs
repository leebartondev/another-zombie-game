using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class SpiderBossAnimation : BossAnimation {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private GameObject spawnDirtPE;

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    public void TriggerSpawnStart () {
        if (Util.IsNull(this.animator) && !this.referencesSetup) {
            this.SetupReferences();
            this.referencesSetup = true;
        }

        if (!this.spawnDirtPE.activeInHierarchy) {
            this.spawnDirtPE.SetActive(true);
        }

        this.animator.enabled = true;
        this.animator.SetTrigger(MyAnimation.SPAWN_START);
    }

    public void TriggerSpawnComplete () {
        this.SpawnAnimationComplete = true;
        this.spawnDirtPE.SetActive(false);
        this.animator.SetTrigger(MyAnimation.SPAWN_COMPLETE);
        this.animator.enabled = false;
    }
}
