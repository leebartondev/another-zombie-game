using UnityEngine;
using UnityEngine.AI;

// Lee Barton
namespace Novasloth.SpiderBoss {
    public class SpiderBossAI : MonoBehaviour {

        private const string ANIM_PASSIVE_ATTACK = "DoPassiveAttack";

        /////////////////////////////////////////////////////////////////
        // P R I V A T E   V A R I A B L E S
        /////////////////////////////////////////////////////////////////

        [SerializeField] private Transform spawnPointsParent;

        private Transform playerTransform;
        private Animator spiderAnimator;
        private NavMeshAgent spiderNavAgent;

        private StateMachine _StateMachine;
        private SpiderBossMovement _SpiderBossMovement;
        private SpiderBossAttack _SpiderBossAttack;
        private SpiderBossHealth _SpiderBossHealth;
        private SpiderBossAnimation _SpiderBossAnimation;

        private bool canRetreat;
        private bool passiveAttackComplete;

        /////////////////////////////////////////////////////////////////
        // U N I T Y   M E T H O D S
        /////////////////////////////////////////////////////////////////

        private void Awake () {
            this.RegisterEventHandlers();
            this.SetupReferences();
            this.SetupState();
        }

        private void Update () {
            this._StateMachine.Tick();
        }

        private void OnDestroy () {
            this.UnregisterEventHandlers();
        }

        /////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        /////////////////////////////////////////////////////////////////

        private void SetupReferences () {
            this._SpiderBossMovement = this.GetComponent<SpiderBossMovement>();
            this._SpiderBossAttack = this.GetComponentInChildren<SpiderBossAttack>();
            this._SpiderBossHealth = this.GetComponentInChildren<SpiderBossHealth>();
            this._SpiderBossAnimation = this.GetComponentInChildren<SpiderBossAnimation>();

            this.spiderAnimator = this.GetComponentInChildren<Animator>();
            this.spiderNavAgent = this.GetComponentInChildren<NavMeshAgent>();

            this.playerTransform = Util.GetPlayerByTag().transform;
        }

        private void SetupState () {
            this._StateMachine = new StateMachine();

            // @TODO add spawn & death states
            IState spawn = new Spawn(this);
            IState lookAtPlayer = new LookAtPlayer(this);
            IState spitAttack = new SpitAttack(this);
            IState retreat = new Retreat(this);
            IState retreatAgain = new RetreatAgain(this);
            IState passiveAttack = new PassiveAttack(this);
            IState death = new Death(this);

            this._StateMachine.AddTransition(spawn, lookAtPlayer,
                () => this._SpiderBossAnimation.SpawnAnimationComplete
            );
            this._StateMachine.AddTransition(lookAtPlayer, spitAttack,
                () => this._SpiderBossAttack.PlayerInRange
            );
            this._StateMachine.AddTransition(spitAttack, lookAtPlayer,
                () => !this._SpiderBossAttack.PlayerInRange
            );
            this._StateMachine.AddTransition(spitAttack, retreat,
                () => this.canRetreat
            );
            this._StateMachine.AddTransition(retreat, retreatAgain,
                () => this._SpiderBossMovement.ReachedRetreatDestination() && this._SpiderBossAttack.PlayerInRange
            );
            this._StateMachine.AddTransition(retreatAgain, retreat,
                () => this._SpiderBossMovement.ReachedRetreatDestination() && this._SpiderBossAttack.PlayerInRange
            );
            this._StateMachine.AddTransition(retreat, passiveAttack,
                () => this._SpiderBossMovement.ReachedRetreatDestination() && !this._SpiderBossAttack.PlayerInRange
            );
            this._StateMachine.AddTransition(retreatAgain, passiveAttack,
                () => this._SpiderBossMovement.ReachedRetreatDestination() && !this._SpiderBossAttack.PlayerInRange
            );
            this._StateMachine.AddTransition(passiveAttack, spitAttack,
                () => this.passiveAttackComplete && !this._SpiderBossAttack.PlayerInRange
            );
            this._StateMachine.AddTransition(passiveAttack, retreat,
                () => this.passiveAttackComplete && this._SpiderBossAttack.PlayerInRange
            );
            this._StateMachine.AddTransition(retreat, spitAttack,
                () => this.passiveAttackComplete && !this._SpiderBossAttack.PlayerInRange
            );
            this._StateMachine.AddTransition(retreatAgain, spitAttack,
                () => this.passiveAttackComplete && !this._SpiderBossAttack.PlayerInRange
            );

            // Any
            this._StateMachine.AddAnyTransition(death,
                () => this._SpiderBossHealth.IsDead()
            );

            // Entry
            this._StateMachine.SetState(spawn);
        }

        public Transform GetRetreatPoint () {
            Transform mostFarTransform = this.spawnPointsParent.GetChild(0);
            float mostFarDistance = 0.0f;
            float distance = 0.0f;

            foreach (Transform spawnPoint in this.spawnPointsParent) {
                distance = Vector3.Distance(this.playerTransform.position, spawnPoint.position);

                if (distance > mostFarDistance) {
                    mostFarDistance = distance;
                    mostFarTransform = spawnPoint;
                }
            }
            Debug.Log(mostFarTransform);
            return mostFarTransform;
        }

        /////////////////////////////////////////////////////////////////
        // I S T A T E   H E L P E R   M E T H O D S
        /////////////////////////////////////////////////////////////////

        // Spawn
        public void StartSpawn () {
            this._SpiderBossAnimation.TriggerSpawnStart();
        }

        public void StopSpawn () {

        }

        // LookAtPlayer, SpitAttack
        public void LookAtPlayer () {
            this._SpiderBossMovement.DoLookAtPlayer();
        }

        // SpitAttack
        public void StartSpitAttack () {
            this.passiveAttackComplete = false;
            this._SpiderBossAttack.AttackPlayer = true;
        }

        public void StopSpitAttack () {
            this._SpiderBossAttack.StopSpitAttack();
        }

        // Retreat
        public void StartRetreat () {
            this.canRetreat = false;
            this._SpiderBossMovement.SetupRetreat();
            this._SpiderBossMovement.DoRetreat();
        }

        public void StopRetreat () {
            this._SpiderBossMovement.StopRetreat();
        }

        // Passive Attack
        public void StartPassiveAttack () {
            this.spiderAnimator.enabled = true;
            this._SpiderBossAttack.PassiveAttack = true;
        }

        public void StopPassiveAttack () {
            this._SpiderBossAttack.StopPassiveAttack();
            this.spiderAnimator.SetBool(ANIM_PASSIVE_ATTACK, false);
            this.spiderAnimator.enabled = false;
        }

        // Death
        public void StartDeath () {
            if (this.spiderAnimator.isActiveAndEnabled) { this.spiderAnimator.enabled = false; }
            if (this.spiderNavAgent.isActiveAndEnabled) { this.spiderNavAgent.enabled = false; }

            this._SpiderBossAttack.enabled = false;
            this._SpiderBossMovement.enabled = false;

            this.enabled = false;
        }

        /////////////////////////////////////////////////////////////////
        // E V E N T S
        /////////////////////////////////////////////////////////////////

        private void RegisterEventHandlers () {
            SpiderBossEventHandler.SpiderBossHealthQuarterDamage += this.OnQuarterDamageTaken;
            SpiderBossEventHandler.SpiderBossPassiveAttackComplete += this.OnPassiveAttackComplete;
        }

        private void UnregisterEventHandlers () {
            SpiderBossEventHandler.SpiderBossHealthQuarterDamage -= this.OnQuarterDamageTaken;
            SpiderBossEventHandler.SpiderBossPassiveAttackComplete -= this.OnPassiveAttackComplete;
        }

        private void OnQuarterDamageTaken (float currentHealth) {
            this.canRetreat = true;
        }

        private void OnPassiveAttackComplete () {
            this.passiveAttackComplete = true;
        }

    }
}
