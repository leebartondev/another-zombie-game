using UnityEngine;
using Novasloth;

// Novasloth Games LLC
// Lee Barton
public abstract class EnemyAttack : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] protected int attackDamage = 10;
    [SerializeField] protected float attackDelay = 0.5f;

    protected bool inRangeToAtack = false;
    protected Timer attackTimer;
    protected bool timeToAttack;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected abstract void Awake ();

    protected abstract void Start ();

    protected abstract void Update ();

    // @TODO
    // Use the nav controller to check the current target
    // and base if the zombie should attack based on their
    // current target
    protected abstract void OnTriggerEnter (Collider other);

    protected abstract void OnTriggerExit (Collider other);

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected abstract void SetupReferences ();
    protected abstract void Init ();
    protected abstract void CheckAttack ();
    public abstract void DoAttack ();

    protected virtual void ResetTimer () {
        attackTimer.Reset();
        timeToAttack = false;
    }
}
