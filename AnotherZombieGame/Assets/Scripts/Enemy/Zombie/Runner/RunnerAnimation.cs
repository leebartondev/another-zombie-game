using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class RunnerAnimation : ZombieAnimation {

    /////////////////////////////////////////////////////////////////
    // G L O B A L S  /  C O N S T A N T S
    /////////////////////////////////////////////////////////////////

    public static float RUN_SPEED = 2.65f;
    public static float SPRINT_SPEED = 6.5f;

    public static float SPRINT_CHANCE = 0.5f; // 50%

    // Start is called before the first frame update
    protected override void Start () {
        base.Start();
        SetAnimationAndNavSpeed();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    protected override void SetAnimationAndNavSpeed () {
        bool isSprinting = Util.GetChance(SPRINT_CHANCE);
        this.animator.SetBool(MyAnimation.IS_SPRINTING, isSprinting);
        this._EnemyMovement.SetNavMeshSpeed(isSprinting ? SPRINT_SPEED : RUN_SPEED);
    }
}