using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Lee Barton
public interface IState {
    void OnEnter(); //
    void Tick ();    // MonoBehaviour.Update() -> StateMachine.Tick() -> IState.Tick()
    void OnExit();  //
}
