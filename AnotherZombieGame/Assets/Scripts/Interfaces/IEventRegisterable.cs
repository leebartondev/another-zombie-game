﻿// Novasloth Games LLC
// Lee Barton
public interface IEventRegisterable {
    void RegisterEventHandlers ();
    void UnregisterEventHandlers ();
}
