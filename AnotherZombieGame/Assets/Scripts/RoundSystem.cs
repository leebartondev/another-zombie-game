using UnityEngine;
using Novasloth;

// Novasloth Games LLC
// Lee Barton
public class RoundSystem : MonoBehaviour, IEventRegisterable {

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public int CurrentRound { get { return currentRound; } }

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [Header("Round System Properties")]
    [SerializeField] private int currentRound;
    [SerializeField] private float roundDelay = 5.0f;

    [Header("Debug")]
    [SerializeField] private bool debug = false;

    private Spawner _Spawner;
    private HUD _HUD;

    private Timer roundDelayTimer;
    private bool timeToStartRound = false;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        RegisterEventHandlers();
        SetupReferences();
        Init();
    }

    private void Start () {
        roundDelayTimer.Pause();
    }

    private void Update () {
        roundDelayTimer.Tick();

        CheckRound();
    }

    private void OnDestroy () {
        UnregisterEventHandlers();
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void SetupReferences () {
        GameObject spawner = Util.GetSpawnerByTag();
        GameObject player = Util.GetPlayerByTag();

        if (Util.IsNull(player)) {
            Debug.LogError("RoundSystem :: SetupReferences() :: Player is NULL! Disabling...");
            enabled = false;
            return;
        }

        _HUD = player.GetComponentInChildren<HUD>();

        if (Util.IsNull(spawner)) {
            Debug.LogError("RoundSystem :: SetupReferences() :: Spawner is NULL!");
            return;
        }

        _Spawner = spawner.GetComponent<Spawner>();
    }

    private void Init () {
        if (!debug) {
            currentRound = 0;
        }

        roundDelayTimer = new Timer(roundDelay, () => {
            timeToStartRound = true;
        });
    }

    // Check if new round should begin
    private void CheckRound () {
        if (!_Spawner.Spawning) {

            // Should be new round, but haven't started delay
            if (_Spawner.EnemyCount == 0 && !roundDelayTimer.IsStarted) {
                // Debug.Log($"INFO :: RoundSystem :: Starting delay timer...");
                roundDelayTimer.Start();
            }

            // Should be new round and delay is over
            if (_Spawner.EnemyCount == 0 && timeToStartRound) {
                // Debug.Log($"INFO :: RoundSystem :: Starting new round...");
                NewRound();
                timeToStartRound = false;
            }
        }
    }

    // Start a new round spawning zombies.
    private void NewRound () {
        currentRound++;
        // Debug.Log($"INFO :: RoundSystem :: New Round {this.currentRound}");
        _HUD.NewRound(currentRound);
        _Spawner.NewRoundSpawn(currentRound);
        GameEventHandler.NewRound(currentRound);

        roundDelayTimer.Reset();
        roundDelayTimer.Pause();
    }

    /////////////////////////////////////////////////////////////////
    // E V E N T   H A N D L E R S
    /////////////////////////////////////////////////////////////////

    public void RegisterEventHandlers () {
        PlayerEventHandler.PlayerDeathEvent +=
            OnPlayerDeathEvent;
    }

    public void UnregisterEventHandlers () {
        PlayerEventHandler.PlayerDeathEvent -=
            OnPlayerDeathEvent;
    }

    private void OnPlayerDeathEvent () {
        enabled = false;
    }
}