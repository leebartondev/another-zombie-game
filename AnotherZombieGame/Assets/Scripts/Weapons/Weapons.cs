﻿using System.Collections.Generic;
using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public sealed class Weapons {

    /////////////////////////////////////////////////////////////////
    // E N U M
    /////////////////////////////////////////////////////////////////

    public enum Enum {
        PISTOL, // 0
        ADVANCED_PISTOL, // 1
        ASSAULT_RIFLE, // 2
        SHOTGUN, // 3
        SUBMACHINE_GUN, // 4
        LIGHT_MACHINE_GUN, // 5
        GRENADE, // 6
        ROCKET_LAUNCHER, // 7
        ALIEN_BLASTER // 8
    }

    public enum Kind {
        PISTOL, // 0
        RIFLE, // 1
        LAUNCHER, // 2
        THROWABLE // 3
    }

    /////////////////////////////////////////////////////////////////
    // I N S T A N C E
    /////////////////////////////////////////////////////////////////

    private static readonly Weapons instance = new Weapons();

    // Explicit static constructor to tell C# compiler to not mark
    // type as 'beforefieldinit'
    static Weapons () {}
    private Weapons () {}

    public static Weapons Instance {
        get {
            return instance;
        }
    }

    /////////////////////////////////////////////////////////////////
    // G L O B A L S  /  C O N S T A N T S
    /////////////////////////////////////////////////////////////////

    public static float TIMER_CONVERSION = 1000.0f;

    public static int SHOOTABLE_MASK = LayerMask.GetMask(
        LayerMask.LayerToName((int)Layer.SHOOTABLE)
    );

    // Mapped to Enum
    private static int[] PRICES = new int[8] {
        0,
        5000,
        7500,
        8000,
        8000,
        9000,
        9500,
        10000
    };

    /////////////////////////////////////////////////////////////////
    // P U B L I C   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    public Enum SelectedWeapon { get; set; } = Enum.PISTOL;
    public bool HasPistol { get; set; } = true;
    public bool HasRifle { get; set; } = false;
    public bool HasThrowable { get; set; } = false;
    public float DamageModifier { get; set; } = 0.0f;
    public float RateOfFireModifier { get; set; } = 0.0f;
    public float SpreadModifier { get; set; } = 0.0f;
    public float ExplosionRadiusMultiplier { get; set; } = 1.0f;
    public float ExplosionDamageMultiplier { get; set; } = 0.0f;

    /////////////////////////////////////////////////////////////////
    // G E T T E R S
    /////////////////////////////////////////////////////////////////

    // Get weapon price based on weapon type.
    public static int GetWeaponPrice (Weapons.Enum weaponType) => PRICES[(int)weaponType];

    // Get weapon price based on index.
    public static int GetWeaponPrice (int index) => PRICES[index];

    // Get the prices array length
    public static int GetWeaponPricesLength () => PRICES.Length;

    // Get the number of weapons listed in Enum.
    // @TEST
    public static int GetWeaponsCount () => System.Enum.GetNames(typeof(Weapons.Enum)).Length;

    /////////////////////////////////////////////////////////////////
    // S E T T E R S
    /////////////////////////////////////////////////////////////////

    // Set that the player is holding a pistol weapon.
    public void SetPlayerHasPistol () {
        this.HasPistol = true;
        this.HasRifle = false;
        this.HasThrowable = false;
    }

    // Set that the player is holding a rifle weapon.
    public void SetPlayerHasRifle () {
        this.HasPistol = false;
        this.HasRifle = true;
        this.HasThrowable = false;
    }

    // Set thtat the player is holding a launcher weapon.
    public void SetPlayerHasLauncher () {
        this.HasPistol = false;
        this.HasRifle = true;
        this.HasThrowable = false;
    }

    // Set that the player is holding a throwable weapon.
    public void SetPlayerHasThrowable () {
        this.HasPistol = false;
        this.HasRifle = false;
        this.HasThrowable = true;
    }

    // Set kind of weapon based on weapon type.
    public void SetPlayerIsHoldingKind (Weapons.Kind kind) {
        if (kind == Weapons.Kind.PISTOL) {
            this.SetPlayerHasPistol();
        } else if (kind == Weapons.Kind.RIFLE) {
            this.SetPlayerHasRifle();
        } else if (kind == Weapons.Kind.LAUNCHER) {
            this.SetPlayerHasLauncher();
        } else if (kind == Weapons.Kind.THROWABLE) {
            this.SetPlayerHasThrowable();
        }
    }

    /////////////////////////////////////////////////////////////////
    // S T A T I C   H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    // Convert weapon rate of fire to timer delay float.
    public static float RateOfFireToTimerDelay (float rateOfFire) {
        return ((rateOfFire / TIMER_CONVERSION) / (1.0f + Instance.RateOfFireModifier));
    }

    // Calculate damage with perk modifier.
    public static float ModifiedDamage (float baseDamage) {
        return (baseDamage * (1.0f + Instance.DamageModifier));
    }

    // Return a new ray from weapon's muzzle to a specified direction.
    public static Ray CreateWeaponRay (float spread, float range, Vector3 muzzlePosition, Transform playerTransform)
        => new Ray(
            muzzlePosition,
            CalculateSpread(spread, range, playerTransform)
        );

    // Check if a raycast from weapon hits specified layer.
    public static bool RayHitShootableMaskLayer (Ray weaponRay, out RaycastHit hitInfo, float rayMaxDistance)
        => Physics.Raycast(weaponRay, out hitInfo, rayMaxDistance, SHOOTABLE_MASK, QueryTriggerInteraction.Ignore);

    // Calculate spread of bullets. TEST?
    public static Vector3 CalculateSpread (float spread, float range, Transform directionTransform)
        => Vector3.Lerp(
            directionTransform.TransformDirection(Vector3.forward * range),
            Random.onUnitSphere,
            (spread * (1.0f - Instance.SpreadModifier))
        );

    // Check if enough time has passed to disable effects.
    public static bool IsTimeToDisableEffects (float delayTimer, float delayTime, float displayTime)
        => (delayTimer >= (delayTime * displayTime));

    // Check that the specified weapon
    // @TEST
    public static bool IsLocked (Enum weapon, int brains)
        => (brains < GetWeaponPrice((int)weapon));

    // @TEST
    public static bool IsLocked (Enum weapon, List<Purchasing.State> state)
        => (state[(int)weapon] == Purchasing.State.LOCKED);

    // Check that the specified weapon can be purchased.
    public static bool IsPurchasable (Enum weapon, int brains)
        => (brains >= GetWeaponPrice((int)weapon));

    // @TEST
    public static bool IsPurchasable (Enum weapon, List<Purchasing.State> state)
        => (state[(int)weapon] == Purchasing.State.PURCHASABLE);

    // @TEST
    public static bool IsPurchased (Enum weapon, List<Purchasing.State> state)
        => (state[(int)weapon] == Purchasing.State.PURCHASED);

    // Check provided index is less than last weapon's index in Enum.
    // @TEST
    public static bool IndexLessThanLastPurchasableWeapon (int weaponIndex)
        => (weaponIndex < (GetWeaponsCount() - 1));

    // Check provided index is greater than last weapon's index in Enum.
    // @TEST

    // Check provided index is greater than first purchasable weapon's index in Enum.
    // @TEST
    public static bool IndexGreaterThanFirstPurchasableWeapon (int weaponIndex)
        => (weaponIndex > ((int)Enum.ADVANCED_PISTOL));

    // Check provided index is less than first purchasable weapon's index in Enum.
    // @TEST

    // Check if two weapon types are equal.
    public static bool IsEqual (Enum first, Enum second) => (first == second);

    // Check that ammo count means infinite ammo.
    public static bool IsInfiniteAmmo (int ammo) => (ammo >= 9999);

    // Check that weapon ammo is full.
    public static bool IsFullAmmo (int ammo, int maxAmmo) => (ammo == maxAmmo);

    // Check that the weapon has ammo.
    public static bool IsNotEmpty (int ammo) => (ammo > 0);
}
