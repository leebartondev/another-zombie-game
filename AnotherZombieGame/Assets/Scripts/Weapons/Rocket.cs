﻿using UnityEngine;

// Novasloth Games LLC
// Lee Barton
public class Rocket : MonoBehaviour {

    /////////////////////////////////////////////////////////////////
    // P R I V A T E   V A R I A B L E S
    /////////////////////////////////////////////////////////////////

    [SerializeField] private GameObject rocketParticleEffects;

    private Rigidbody rigidBody;
    private Explode _Explode;

    /////////////////////////////////////////////////////////////////
    // U N I T Y   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void Awake () {
        SetupReferences();
    }

    private void Start () {
        RigidbodyIsActive(false);
    }

    /////////////////////////////////////////////////////////////////
    // H E L P E R   M E T H O D S
    /////////////////////////////////////////////////////////////////

    private void SetupReferences () {
        rigidBody = GetComponent<Rigidbody>();
        _Explode = GetComponent<Explode>();
    }

    public void Fire (float spread, float force, Transform direction, float radiusMultiplier) {
        transform.SetParent(null);
        RigidbodyIsActive(true);
        Vector3 myVector = Weapons.CalculateSpread(spread, 500f, direction);
        Debug.DrawLine(transform.position, myVector, Color.red, 10f, true);
        transform.LookAt(myVector);
        rigidBody.AddForce(direction.forward * force);
        DoExplode(radiusMultiplier);
    }

    private void DoExplode (float radiusMultiplier) {
        if (rocketParticleEffects != null) {
            rocketParticleEffects.SetActive(true);
        }

        if (_Explode != null) {
            _Explode.RadiusUpgrade = radiusMultiplier;
            _Explode.enabled = true;
        }
    }

    private void RigidbodyIsActive (bool active) {
        rigidBody.isKinematic = !active;
        rigidBody.detectCollisions = active;
    }
}
